
# დინამიური ფროფერთი (Dynamic Property).
---
რა არის დინამიური ფროფერტი?
---
დინამიური ფროფერთი არის ფუნქციონალი Core პროექტში, რომლითაც შეძლებ კოდში ჩარევის გარეშე დაამატო ახალი ველი (ფროფერთი) პროგრამაში.

---
როგორ გამოვიყენოთ დინამიური ფროფერთი (DynamicProperty)?
---
* # Step 1
- მოვძებნოთ Dynamic Property.  "Ctrl + ," საშუალებით.(იხილე სქრინი)

[![](dynamic_property_files/Screenshot_1.png )](dynamic_property_filesScreenshot_1.png )

* # Step 2

- შევქმნათ ახალი ჩანაწერი New ღილაკის დაჭრით. სქრინზე ნაჩვენებია ახალი დინამიური ფროფერთის ჩანაწერი.

[![](dynamic_property_files/Screenshot_2.png )](dynamic_property_files/Screenshot_2.png )

* # Step 3
- Property Type _ს ჩამონათვალში ვირჩევთ ტიპს რომელცი გვინდა რომ იყოს ჩვენი ველი (ფროფერთი)

[![](dynamic_property_files/Screenshot_3.png )](dynamic_property_files/Screenshot_3.png )

* # Step 4
  Get Method Type _შეგვიძლია ავირჩიოთ სამი მნიშვნელობა
  - Value რომელიც პროგრამაში დაამატებს ველს და  Type_ში მითითებულ თეიბლში ამატებს ველს (ქოლუმს) მნიშველობებიც ავტომატურად ჩიწერება ბაზაში.
  - Persistent Alias რომელიც პროგრამაში გამოაჩენს ველს მაგრამ ბაზაში არ ჩაამატებს.
  - Script პროგრამაში დაემატება ველი რომელსაც მიენიჭება სკრიპტიდან დაბრუნებული მნიშვნელობა. ბაზაში არ ჩაიწეერება. 
[![](dynamic_property_files/Screenshot_4.png )](dynamic_property_files/Screenshot_4.png )

* # Step 5
- Lookup Property Type _ში უნდა ავირჩიოთ ის ჩანაწერი რა ტიპისაც გვინდა რომ იყოს ჩვენი ველი.

[![](dynamic_property_files/Screenshot_5.png )](dynamic_property_files/Screenshot_5.png )

  Type _ში ავირჩიოთ ობიექტი რომელზეც გვინდა ველის დამატება.
  Is Active მოვნიშნოთ იმ შემთხვევაში, თუ გვინდა რომ ძალაში შევიდეს ეს ფროფერთი.

[![](dynamic_property_files/Screenshot_6.png )](dynamic_property_files/Screenshot_6.png )


* # Step 6
შენახვის შემდგომ Script_ ში ავტომატურად დაგენერირდება ჩნაწერი, სადაც შეგიძლიათ C# ლოგიკა დაამატოთ, რომელიც ფროფერთიში მნიშვნელობას დააბრუნებს.

[![](dynamic_property_files/Screenshot_7.png )](dynamic_property_files/Screenshot_7.png )

* # Step 7
ველში Persitent Alias Expression-ში შეგვიძლია დავწეროთ ლოგიკა,რომელი ლოგიკითაც განისაზღვრება ამ Property-ს მნიშვნელობა.  
როგორც Step 4-შიც არის აღნიშნული, ეს ველი ბაზში არ დაემატება, მხოდლოს მიმდინარე მდგომარეობის შესაბამის მნიშვენლობას დააბრუნებს.

[![](dynamic_property_files/Screen8.PNG )](dynamic_property_files/Screen8.PNG )
