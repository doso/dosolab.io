# **თანამშრომლის დამატება და Human Resource - ის  შექმნა**
---

* # Step 1

 ### **Core - ის პროგრამიდან შევდივართ მოდულში "Report Definition" და სქრინის მიხედვით ვაჭერთ ღილაკს საიდანაც იქმნება ახალი ჩანაწერი**
---
### **Screen #1**
[![](Report_Definition_Files/Screenshot_1.png ){: width="100%"}](Report_Definition_Files/Screenshot_1.png )
---
---

* # Step 2
### **ახალი ტაბის გახსნის შემდეგ სქრინზე მითითებულ ველში ვარქმევთ რეპორტს სახელს და "New Sql Query" ღილაკის მეშვეობით ვამატებთ რეპორტის ლოგიკას**
--------
### **Screen #2**
[![](Report_Definition_Files/Screenshot_2.png ){: width="100%"}](Report_Definition_Files/Screenshot_2.png )
--------
--------

* # Step 3
### **Sql Query - ს ვარქმევთ სახელს და Query - ში ვწერთ slq სკრიპტს და ვინახავთ**
--------
### **Screen #3**
[![](Report_Definition_Files/Screenshot_3.png ){: width="100%"}](Report_Definition_Files/Screenshot_3.png )
--------
--------

* # Step 4
### **Sql Query - ს შენახვის შემდეგ გადავდივართ ტაბში "Report Definition Group" და სქრინზე მითითებულ ღილაკით ვულინკავთ შესაბამის  "Report Definition Group" - ს.**
--------
### **Screen #4**
[![](Report_Definition_Files/Screenshot_4.png ){: width="100%"}](Report_Definition_Files/Screenshot_4.png )
--------
--------

* # Report Definition Group (Explained)
**#1**
### **Report Definition Group - ი არის პროგრამაში დამატებული მოდული რომელიც აკავშირებს რეპორტებს და როლებს, კერძოდ ამ მოდულის მეშვეობით შეგვიძლია განვსაზღვროთ რომელ როლს რომელი რეპორტის დაგენერირების უფლება ჰქონდეს, მაგალითისთვის განვიხილოთ "Bancassurance Manager - ის ჩანაწერი, სქრინზე ნაჩვენებია საიდან ხდება Report Definition Group - ის ჩანაწერის ნახვა.**
--------
### **Screen #5**
[![](Report_Definition_Files/Screenshot_5.png ){: width="100%"}](Report_Definition_Files/Screenshot_5.png )
--------
--------

**#2**
### **Bancassurance Manager - ის ჩანაწერი მოვძებნოთ Report Definition Group - ის მოდულში და გავხსნათ, Screen #7 - ზე ნაჩვენებია ReportDefinitionCollection - ის ტაბი, აქ დამატებულია რეპორტები რომლებიც უნდა გამოუჩნდეს ჩვენთვის სასურველ როლებს, აქვე შეგვიძლია ჩვენი სურვლის მიხედვით ამოვაკლოთ ან დავამატოთ რეპორტი, Screen #8 -ზე როგორც ჩანს  RolesCollection - ში დამატებულია თანამშრომლის როლები რომლებიც უნდა ხედავდნენ  ReportDefinitionCollection - ში მითითებულ რეპორტებს. Screen #9 - ზე კი ნაჩვენებია ტაბი, სადაც კონკრეტული იუზერის ჩამატებაც შეგვიძლია, თუ გვსურს რომ რეპორტები მასაც გამოუჩნდეს**

--------
### **Screen #6**
[![](Report_Definition_Files/Screenshot_6.png ){: width="100%"}](Report_Definition_Files/Screenshot_6.png )
--------
### **Screen #7**
[![](Report_Definition_Files/Screenshot_7.png ){: width="100%"}](Report_Definition_Files/Screenshot_7.png )
--------
### **Screen #8**
[![](Report_Definition_Files/Screenshot_8.png ){: width="100%"}](Report_Definition_Files/Screenshot_8.png )
--------
### **Screen #9**
[![](Report_Definition_Files/Screenshot_9.png ){: width="100%"}](Report_Definition_Files/Screenshot_9.png )
--------
--------
* # Step 5
### **Report Definition Group - ის ტაბზე დამატების ღილაკის არჩევის შემდეგ გამოვა ფანჯარა სადაც შეგვიძლია მოვძებნოთ ჩვენთვის სასურველი Report Definition Group - ი, ამ შემთხვევაში  Bancassurance Manager, Name - სთან მარჯვენა ღილაკით კონტექსტური მენიუს გამოტანის შემდეგ ავირჩიოთ "Show Auto Filter Row, რაც საშუალებას მოგვცემს ადვილად მოვძებნოთ ჩვენთვის სასურველი Group - ი**
--------
### **Screen #10**
[![](Report_Definition_Files/Screenshot_10.png ){: width="100%"}](Report_Definition_Files/Screenshot_10.png )

--------
--------

* # Step 6
### **მას შემდეგ რაც გამოვა საძიებო ველი რამდენიმე სიმბოლოს მითითების შემდეგ ადვილად მოვძებნით ჩვენთვის სასურველ Report Definition Group - ს, მოძებნის შემდეგ ავირჩევთ Bancassurance Manager - ს და დაემატება Report Definition Groupს Collection  - ში**
--------
### **Screen #11**
[![](Report_Definition_Files/Screenshot_11.png ){: width="100%"}](Report_Definition_Files/Screenshot_11.png )

--------
--------


* # Step 7
### **Report Definition Groupს Collection  - ში სასურველი  Report Definition Group - ის დამატების შემდეგ Save ღილაკის მეშვეობით ვინახავთ ცვლილებებს და გადასამოწმებლად ვაგენერირებთ რეპორტს**
--------
### **Screen #12**
![](Report_Definition_Files/Screenshot_12.png)
--------
### **Screen #13**
![](Report_Definition_Files/Screenshot_13.png)
--------
### **Screen #14**
[![](Report_Definition_Files/Screenshot_14.png ){: width="100%"}](Report_Definition_Files/Screenshot_14.png )
--------
### **Screen #15**
[![](Report_Definition_Files/Screenshot_15.png ){: width="100%"}](Report_Definition_Files/Screenshot_15.png )

--------
--------

* # Step 8
### **დაგენერირების შემდეგ გაიხსნება Desktop - ის ფოლდერი სადაც იქნება ჩვენს მიერ პროგრამიდან წამოღებული რეპორტი Excel - ის ფაილის ფორმატში.**
--------
### **Screen #15**
[![](Report_Definition_Files/Screenshot_16.png ){: width="100%"}](Report_Definition_Files/Screenshot_16.png )

--------
--------