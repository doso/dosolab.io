
Title: ზარალის/სადაზღვევო შემთხვევის გადატანა სხვა პოლისზე 

---

**პერიოდულად დგება საჭიროება რომ უკვე დარეგისტირებული ზარალი/სადაზღვევო შემთხვევა გადავიტანოთ სხვა პოლისზე.**

**ასეთი შემთხვევებისთვის ქვემოთ მოცემულ Command-ებში შესაბამისი სადაზღვევო შემთხვევის ნომრის და პოლისის ნომრის მითითებით შეგვიძლია გადავიტანოთ შემთხვევა სხვა პოლისზე.**

სადაზღვევო შემთხვევა შეიძლება იყოს Motor-ის ან Non Motor-ის ტიპის; 
ქვემოთ მოცემული 2 Command-იდან უნდა გავუშვათ ის, რომელი ტიპის შემთხვევაც გვაქვს.

---


* ზარალში პოლისი გამოცვლა

```sql
with _claimPolicyL as (
select r.id, p.license from reportedclaim r
  join policybase p on p.id = r.policybase
  join insuranceaccident  ia on ia.id = r.insuranceaccident and ia.statementnumber = '______' /*სადაზღვევო შემთხვევის ნომერი*/
)
update reportedclaim as rc
set policybase = (select id from policybase where cp.license = policybase.license and policynumber ilike '______%' /*ახალი პოლისის ნომერი*/)
from _claimPolicyL as cp
where cp.id = rc.id;

```

*მოტორის სადაზღვევო შემთხვევაში პოლისის გამოცვლა:*
```sql
update motorinsuranceaccident as mi
set motorinsurancepackage  = (select id from policybase where policynumber = '______' /*ახალი პოლისის ნომერი*/)
from insuranceaccident as i
  where i.id = mi.id
  and i.statementnumber = '______'  /*სადაზღვევო შემთხვევის ნომერი*/
```

*არა მოტორის სადაზღვევო შემთხვევაში პოლისის გამოცვლა:*
```sql

update nonmotorinsuranceaccident as nmi
set nonmotorinsurancepackage  = (select id from policybase where policynumber = '______' /*ახალი პოლისის ნომერი*/)
from insuranceaccident as i
  where i.id = nmi.id
  and i.statementnumber = '______'  /*სადაზღვევო შემთხვევის ნომერი*/
```

* Base სადაზღვევო შემთხვევაში პოლისის გამოცვლა:

```sql
update insuranceaccident 
set policybase = (select id from policybase where policynumber = '________' /*ახალი პოლისის ნომერი*/)
  where statementnumber = '_______' /*სადაზღვევო შემთხვევის ნომერი*/

```


---

#### ყურადღება მიაქციეთ, რომ პირველი Update-ის ბოლო ნაწილში პოლისის ნომერთან უნდა იყოს %-ის ნიშანი, რომ პოლისის ნომრეის შემცველობით მოხდეს Update-ს გაშვება.

--------
--------
