
# დინამიური ფროფერთი (Dynamic Ipmport).
---
რა არის დინამიური იმპორტი?
---
დინამიური იმპორტი არის ფუნქციონალი Core პროექტში, რომლითაც შეძლებთ კოდში ჩარევის გარეშე დაამატოთ ახალი იმპორტ ფუნქციონალი.

---
როგორ გამოვიყენოთ დინამიური იმპორტი (DynamicIpmport)?
---
* # Step 1
- მოვძებნოთ Dynamic Import.  "Ctrl + ," საშუალებით.(იხილე სქრინი)

![](dynamic_import_files/Screenshot_1.png)

* # Step 2

- შევქმნათ ახალი ჩანაწერი New ღილაკის დაჭრით. სქრინზე ნაჩვენებია ახალი დინამიური იმპორტის ჩანაწერი.

[![](dynamic_import_files/Screenshot_2.png ){: width="100%"}](dynamic_import_files/Screenshot_2.png )

* # Step 3
- Criteria _ში შეგვიძლიათ დავწეროთ ლოგიკა, რომლის მიხედვითაც სკრიპტში დაწერილი ლოგიკა ამუშავდება ან არა. 
მაგალითად თუ გვინდა, რომ მხოლოდ ადმინისტრატორს ჰქონდეს ამ ღილაკის აღზვრის უფლეფა კრიტერიაში ვწერთ ესეთ ლოგიკას:
[CurrentSecuritySystemUser.Roles][[Is Administrative] = True] (იხილე სქრინი)

[![](dynamic_import_files/Screenshot_3.png ){: width="100%"}](dynamic_import_files/Screenshot_3.png )

* # Step 4
- Caption: არ არის სავალდებულო შეგიძლიათ მიუთითო ნებისმიერი დასახელება.
- დასახელება: სავალდებულოა და მიუთითეთ ლოგიკური იმპორტის დასახელება.
 
[![](dynamic_import_files/Screenshot_4.png ){: width="100%"}](dynamic_import_files/Screenshot_4.png )

* # Step 5
- Type: აქ აირჩიეთ ობიექტი, რომელზეც გინდათ რომ გამოჩნდეს იმპორტის ღილაკი.

[![](dynamic_import_files/Screenshot_5.png ){: width="100%"}](dynamic_import_files/Screenshot_5.png )

 * # Step 6
Script_ ში დავამატოთ იმპორტის ლოგიკა სურათზე ნაჩვენებია მაგალითი.

[![](dynamic_import_files/Screenshot_6.png ){: width="100%"}](dynamic_import_files/Screenshot_6.png )


* # Step 7
 შენახვის შემდგომ არჩეულ ობიექტზე გამოჩნება ღილაკი, რომელშიც იქნება იმპორტ ფაილის დაგენერირება და ასევე იმპორტ ფაილის ატვირთვა.(იხილე სქრინზე)

 [![](dynamic_import_files/Screenshot_7.png ){: width="100%"}](dynamic_import_files/Screenshot_7.png )