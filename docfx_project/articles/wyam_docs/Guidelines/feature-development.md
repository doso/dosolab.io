<!----- Conversion time: 1.777 seconds.


Using this Markdown file:

1. Cut and paste this output into your source file.
2. See the notes and action items below regarding this conversion run.
3. Check the rendered output (headings, lists, code blocks, tables) for proper
   formatting and use a linkchecker before you publish this page.

Conversion notes:

* Docs to Markdown version 1.0β16
* Wed Mar 06 2019 05:16:57 GMT-0800 (PST)
* Source doc: https://docs.google.com/a/doso-insurance-systems.com/open?id=18ZL03qRC_6a56MRo1DQr9nu06HuBjzSnG4Oxn8CCCwU
* This document has images: check for >>>>>  gd2md-html alert:  inline image link in generated source and store images to your server.
----->

## Feature Development - GIT Workflow
### UPDATED ON 2019-Mar-06
### Author: Petre Chitashvili
 
0. Prune remote branches - წაშალე არ არსებული რემოუთ ბრენჩები
    *   **git remote prune origin**

1. Pull to update your local master
    *   **git pull origin master**
2. Check out a feature branch && push to remote
    *   **git checkout -b 3275-add-commenting**  

![](feature-development-files/image3.png)
![](feature-development-files/image2.png)
![](feature-development-files/image1.png)

3. Do work in your feature branch, committing and pushing early and **often** - იმუშავე ახალ ბრენჩზე (არა master-ზე!), დააკომიტე და და-push-ე ადრე და ხშირად
    *   **git commit -a -m 'კომიტის მესიჯი'**

        * **მესიჯის სტრუქტურა:**  
        ---
            **სექცია1** სავალდებულო: შეაჯამე ცვლილებები 50 ან ნაკლებ სიმბოლოში | Summarize changes in around 50 characters or less
        ---
            **სექცია2** არასავალდებულო: დეტალური აღწერა, თუ საჭიროა. 72 ან ნაკლები სიმბოლო. ზედა ნაწილი იყოს subject, ეს იყოს ტანი. აუცილებელია იყოს გამოტოვებული ხაზი subject-სა და ტანს შორის (თუ საერთოდ არ არის ტანი გამოტოვებული); 
            More detailed explanatory text, if necessary. Wrap it to about 72 characters or so. In some contexts, the first line is treated as the subject of the commit and the rest of the text as the body. The blank line separating the summary from the body is critical (unless you omit the body entirely); 
        ---
            **სექცია3** არასავალდებულო: აღწერე პრობლემა, რომელსაც ეს კომიტი აგვარებს. ყურადღება გაამახვილე იმაზე, რატომ შეგაქვს ეს ცვლილება ნაცვლად იმისა, რომ აღწერო როგორ ხდება ცვლილება (კოდში ჩანს როგორ). არსებობს გვერდითი ეფექტები ან სხვა არაინტუიტიური შედეგები, რომლებიც შეიძლება ამ ცვლილებამ გამოიწვიოს? აქ შეგიძლია მათზე საუბარი. | Explain the problem that this commit is solving. Focus on why you are making this change as opposed to how (the code explains that). Are there side effects or other unintuitive consequences of this change? Here's the place to explain them.

            - შესაძლებელია bullet point-ების გამოყენება | Bullet points are okay, too

            - bullet-ებად გამოვიყენოთ - ან *, მათ წინ ერთი ` ` სფეისით | Typically a hyphen or asterisk is used for the bullet, preceded a single space
        ---
            **Issue/ები** სავალდებულო: issue-ების/ticket-ების ლინკები ბოლოში | put references to issues at the bottom, like this:

            Resolves: #123
            See also: #456, #789**
        --
        * References
            * [https://chris.beams.io/posts/git-commit/](https://chris.beams.io/posts/git-commit/)
            * [https://about.gitlab.com/2016/03/08/gitlab-tutorial-its-all-connected/](https://about.gitlab.com/2016/03/08/gitlab-tutorial-its-all-connected/)
4. Rebase frequently to incorporate upstream changes
    *   **git fetch origin master && git rebase origin/master**
5. When you complete working on the issue, create a **merge request** from the feature branch to **master** branch on GitLab
![alt_text](feature-development-files/image4.png "image_tooltip")

6. Prune remote branches - წაშალე არ არსებული რემოუთ ბრენჩები
    *   **git remote prune origin**