Title: Remove a file from a git repository
---

# Untrack a file from a git repository to prevent conflicts

* `git rm -r --cached some-directory` **OR** `git rm -r --cached some-file`
    * Sample: `git rm -r --cached PotentialClients.Win/ConnectionStrings.config`
* `git commit -m 'Remove the now ignored "file-OR-directory"'`



## Links:
https://stackoverflow.com/questions/7927230/remove-directory-from-remote-repository-after-adding-them-to-gitignore