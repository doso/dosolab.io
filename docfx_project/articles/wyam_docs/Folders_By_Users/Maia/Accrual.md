
Title: დარიცხვის პროცესში DoSo-ს ჩართულობა

---
      საოპერაციო პროცესში ჩართულობა
      საოპერაციო ცვლილებების დასრულების შემდეგ გადაზღვევების შექმნა
      გადაზღვევების გადამოწმება
      დარიცხვის გაშვება ბაზის ადმინთან კომუნიკაციით
      დარიცხვის შედეგების გადამოწმება


ავტომატური პროცესი შედგება რამდენიმე ეტაპისგან
#

# საოპერაციო პროცესში ჩართულობა
**ბანკი რესპუბლიკის პორტფელური სიცოცხლის და ქონების დაზღვევის ჩანაწერების იმპორტი**
- მარი თავართქილაძე გვიგზავნის Excel-ის ფაილებს
- ვამზადებთ იმპორტ ფაილს
- ვაიმპორტებთ პროგრამაში
- შექმნილ ჯამურ პოლისებს ვამოწმებ, ვავალიდურებ და ვუცვლი პოლისის ნომერს (არსებულ პოლისის ნომრებს ბოლოში ვუმატებ ‘-BR’ პაკეტსაც და პოლისსაც)

```sql
update policybase set policynumber = policynumber||'-BR' where policynumber ilike '___%'
```
**ვშლი თვის განმავლობაში საცდელად დაგენერირებულ TBC-ის ჯამურ პოლისებს**
- ვააფდეითებ მანსლი პოლისებს რომლის ჯამური პოლისი არის Null ან ჯამური პოლისი არის წაშლილი
```sql
update monthlypolicy --asd
set isdeleted = True 
where id in (
 select m.id from monthlypolicy m
   left join policybase p on p.id = m.insurancepackage
 where (p.expiredon is not null or p.id is null) and m.start_date = '2017-12-01'
            )
and isdeleted !=True
```
#
# ბორდეროების წამოღება
დასამატებელია

- მანსლი პოლისების წამოღების (Life და LifeMicro) შემდეგ ვუშებ შემდეგ აფდეითს, რომ გამოვასწორო ლოგიკაში გაპარული ხარვეზი

```sql
update monthlypolicy set
 parisk = weighted_average_loan_balance,
 papremium = insurance_premium,
 liferisk = 0,
 lifepremium = 0,
 lifenetpremium = 0,
 lifecommission = 0,
 panetpremium = (weighted_average_loan_balance * 0.1728) / 365 * (datediff('day', insurance_period_start, insurance_period_end) + 1)/100,
 pacommission = insurance_premium - (weighted_average_loan_balance * 0.1728) / 365 * (datediff('day', insurance_period_start, insurance_period_end) + 1)/100
where liferisk < 0 and start_date = '2017-12-01'
```
- ვაგენერირებ ბორდეროს რეპორტებს **Life Bordereaux** და **Property Bordereaux**, მოცულობის სიდიდის გამო გაგზავნა არ ხერხდება და მარი თავართქილაძეს ვუდებ Dev სერვერის ერთ-ერთ ფოლდერში
- ამის შემდეგ მარი მიგზავნის დადასტურებული ბორდეროს ჯამურ ციფრებს, ვაკორექტირებ პოლის(ებ)ს და ვავალიდურებ.
  - Life - ვცვლი შემდეგს:
     - პროდუქტი (რომელიც გამოტანილია PA-ის გრუპში) - Life ან Life Micro, მასში შემავალი Monthly Policy-ების Product ID-ის მიხედვით 
     - გადახდის ჯერადობა - ერთჯერადი
    - Life სადაზღვევო თანხა - მოწოდებულის მიხედვით
    - Life სადაზღვევო პრემია - მოწოდებულის მიხედვით
    - PA სადაზღვევო თანხა - მოწოდებულის მიხედვით
    - PA სადაზღვევო პრემია - მოწოდებულის მიხედვით
    - ვავალიდურებ
  - Property - ვცვლი: 
    - პროდუქტი (რომელიც გამოტანილია პოლისის ნაწილში პოლისის  N-ის ქვემოთ) - P ortfolioProperty
    - გადახდის ჯერადობა - ერთჯერადი
    - სადაზღვევო თანხა - მოწოდებულის მიხედვით
    - სადაზღვევო პრემია - მოწოდებულის მიხედვით
    - ვავალიდურებ

# გადაზღვევების შექმნა და გადამოწმება
- პირვე რიგში ვქმნი **პროპორციულ** გადაზღვევებს 

დასამატებელია როგორ იქმენება

- ამის შემდეგ იწყება გადამოწმება:
  - ხომ არ არის წაშლილი რომელიმე გადაზღვევა ისეთ პოლისზე რომლიც არ არის წაშლილი, სელექტის მიერ დაბრუნებულ გადაზღვევებს გადავამოწმებ და აღვადგენ
```sql
  WITH RECURSIVE cte AS (
    SELECT
      p.id,
      p.policynumber
    FROM policybase p
      JOIN reinsuredpolicy r ON r.insurancepolicy = p.id
    WHERE r.expiredon IS NOT NULL
          AND p.expiredon IS NULL
          AND p.createdon >= '2017-07-01'
          AND coalesce(p.cancellationdate, now()) != p.fromdate
          AND p.policyisvalid )
  , activereins AS (
    SELECT
      p.id,
      p.policynumber
    FROM policybase p
      JOIN cte c ON c.id = p.id
      JOIN reinsuredpolicy r ON r.insurancepolicy = p.id AND r.expiredon IS NULL )  
      SELECT * FROM cte c   
      LEFT JOIN activereins a ON a.id = c.id 
      WHERE a.id IS NULL
```

დასამატებელია როგორ იქმენება

    - ვამოწმებ არის თუ არა შემთხვევა სადაც პოლისის გაუქმების თარიღი განსხვავდება გადაზღვევის გაუქმების თარიღისგან, თუ აღმოჩნდება ასეთი პოლისი რეინსის გაუქმების თარიღში გადავიტან პოლისის გაუქმების თარიღს

```sql
  SELECT p.id, p.policynumber, p.effectivedate, p.cancellationdate, r.cancelationdate
FROM policybase p
JOIN reinsuredpolicy r ON r.insurancepolicy = p.id and r.expiredon is null
WHERE p.expiredon IS NULL
    AND p.createdon > '2017-06-01'
    and coalesce(p.cancellationdate::date, now()) != p.fromdate::date
    AND coalesce(p.cancellationdate::Date, now()::Date) != coalesce(r.cancelationdate, now()::date)
```
აფდეითის ქვერი

```sql
with cte as (
   SELECT
     p.id,
     p.policynumber,
     p.effectivedate,
     p.cancellationdate,
     r.cancelationdate
   FROM policybase p
     JOIN reinsuredpolicy r ON r.insurancepolicy = p.id and r.expiredon is null
   WHERE p.expiredon IS NULL
         AND p.createdon > '2017-06-01'
         and coalesce(p.cancellationdate :: date, now()) != p.fromdate :: date
         AND coalesce(p.cancellationdate :: Date, now() :: Date) != coalesce(r.cancelationdate, now() :: date)
)
update reinsuredpolicy as r
 set cancelationdate = ct.cancellationdate
 from cte as ct
where ct.id = r.insurancepolicy and r.expiredon is null
```



#

# გაუქმების მოთხოვნის შექმნა
**2. Letter-ის საფუძველზე ხდება გაუქმების მოთხოვნის დაგენერირება**
- უნდა არსებობდეს აქტიური წერილი
- რომლის შეტყობინების სტატუსში მითითებულია - ჩაბარებული
   - ჩაბარებულია სტატუსი SMS-ით გაგზავნის შემთხვევაში ენიჭება ავტომატურად SMS-ის გაგზავნისთანავე
- ამ წერილის საფუძველზე ჯერ არ უნდა იყოს შექმნილი გაუქმების მოთხოვნა
.
# Email შეტყობინება (დავალიანების დადგომა)
**3. გაუქმების მოთხოვნის შექმნისას Email შეტყობინება ეგზავნება შემდეგ პასუხისმგებელ პირებს, თანამშრომლის ჩანაწერში მითითებულ ელ. ფოსტის მისამართზე**
- პოლისში მითითებული პასუხისმგებელი (რომლის ელ. ფოსტის მისამართი შეიცავს 'tbcbank', 'kopenbur', 'tbcinsurance',
'constanta')
- პასუხისმგებლის უფროსთან
- თუ მიმდინარე უფროსი არ ჰყავს, მაგ. აგენტი აღარ არის თანამშრომელი, მაშინ გაიგზავნება ბოლო უფროსთან
- თუ პოლისის მიმართულება არის Bancassurance მაშინ გაიგზავნება შემდეგ მისამართებზე
   - sergi.khizanishvili@tbcinsurance.ge
   - NuChkuaseli@tbcbank.com.ge
   - Mnaroushvili@tbcbank.com.ge
- თუ მიმართულება არ არის Bancassurance მაშინ გაიგზავნება შემდეგ მისამართებზე
   - Ivane.Kakhadze@tbcinsurance.ge
   - Tamar.Tskhviravashvili@tbcinsurance.ge
   - თუ გაუქმების ინიციატორი არის - დამზღვევი - gmegeneishvili@kopenbur.ge
   - თუ მიმართულება არის - Channel - achaladze@kopenbur.ge
   - თუ არხი არის WestGeorgia - jabdaladze@kopenbur.ge
- Bancassurance მიმართულების მქონე პოლისზე გაგზავნილ შეტყობინებას შემდეგი სახე აქვს

![](Screens/EmailScreen.png)

- თუ მიმართულება არ არის Bancassurance შემდეგი

![](Screens/EmailScreen2.png)
.
#

# დავალიენბის გადამოწმება და მოთხოვნის გაუქმება
**4. გაუქმების მოთხოვნის დარეგისტრირებიდან მე-13 დღეს მოწმდება დავალიანების სტატუსი**
- თუ დავალიანება დაფარულია უქმდება გაუქმების მოთხონა (ენიშნება IsCancelled)
.
#

# Email შეტყობინება (დავალიანების არსებობა)
**5. გაუქმების მოთხოვნის დარეგისტრირებიდან მე-14 დღეს Email შეტყობინება ეგზავნება შესაბამის (ზემოთ მითითებულ) პასუხისმგებელ პირებს, დავალიანების არსებობის თაობაზე**
.
#
# დავალიენბის გადამოწმება და მოთხოვნის გაუქმება
**6. გაუქმების მოთხოვნის დარეგისტრირებიდან 29-ე დღეს კიდევ ერთხელ მოწმდება მოქმედი გაუქმების მოთხოვნებისთვის დავალიანების სტატუსი**
- თუ დავალიანება დაფარულია უქმდება გაუქმების მოთხოვნა (ენიშნება IsCancelled)
.
#
# პოლისების გაუქმება
**7. გაუქმების მოთხოვნის დარეგისტრირებიდან 30-ე დღეს უქმდება დავალიანების მქონე პოლისები. გაუქმება ხდება შემდეგი ლოგიკის მიხედვით ინფორმაციას ვიღებთ გაგზავნილი წერილების მიხედვით**
- წერილი არ უნდა იყოს წაშლილი
- უნდა ფიქსირდებოდეს ჩაბარების თარიღი (ანუ უნდა იყოს ჩაბარებული/ SMS გაგზავნილი (Delivery status-ს
ვერ ვამოწმებთ))
- პოლისი არ უნდა იყოს გაუქმებული
- არ უნდა იყოს ვადაგასული
- უნდა არსებობდეს გაუქმების მოთხოვნა რომლის
   - გაუქმების სავარაუდო თარიღი არის მიმდინარე დღე
- გაუქმების ინიციატორი არის - მზღვეველი
- არ არის მონიშნული IsCancelled
   - პოლისს უნდა ჰქონდეს გადახდის გრაფიკი რომელიც
   - არ არის წაშლილი
   - მონიშნული აქვს IsActive
   - გრაფიკის თარიღი ნაკლებია გაუქმების მოთხოვნის თარიღზე
   - გრაფიკების ჯამი უნდა იყოს მეტი 1 ერთეულზე