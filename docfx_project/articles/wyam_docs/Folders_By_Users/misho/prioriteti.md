Title: მიშო!
---
# პრიორიტეტები

# Step 1

თქვენს "დესკტოპზე" იქნება ფოლდერი სახელწოდებით doso-insurance-documentation
---
![](Screen/1.png)

# Step 2

შემდგომ შედიხართ Blog_ის ფოლდერში 

![](Screen/2.png)


# Step 3

გახსნით ფაილს მიმდინარე თარიღით (მაუსის მარჯვენა ღილაკის მეშვეობით Edit with VS Code)

![](Screen/3.png)


# Step 4

დაიწყებთ თქვენი პრიორიტეტების შეყვანას 

# სახელი გვარი
</br>

|**#**|**Task Name**|**Helper**| **Finished** 
:-----:|:-----|:-----:|:-----:
 1| პრიორიტეტების შეყვანა| - |<input disabled="disabled" type="checkbox">
 2|
 3|


![](Screen/4.png)
 
 # Step 5

 როცა თქვენ მიერ განსაზღვრულ პრიორიტეტებს დაასრულებთ მონიშნავთ "პწიჩკით" "disabled" გადაიყვანთ

 "enabled"_ში


# სახელი გვარი
</br>

|**#**|**Task Name**|**Helper**| **Finished** 
:-----:|:-----|:-----:|:-----:
 1| პრიორიტეტების შეყვანა| - |<input disabled="enabled" type="checkbox" checked="checked">
 2|
 3|

![](Screen/5.png)


  