# ზარალის დარეგულირებიდან პრემიის გაქვითვა
---
---
#
* # Step 1

 ### Core - ის პროგრამიდან ხარჯის ველში ვამოწმებთ ფიქსირდება თუ არა გაქვითული თანხა
---
![](ClaimSetoffScreenShots/Screenshot_1.png)


---
---

* # Step 2
### თუ თანხა გაქვითულია სქრინზე მითითბული ღილაკით ვხსნით გაქვითვას
--------
![](ClaimSetoffScreenShots/Screenshot_2.png)
-------
-------



* # Step 3
### ვხსნით ხარჯის ჩანაწერს და Expire ღილაკით ვშლით Payable - ს 
--------

![](ClaimSetoffScreenShots/Screenshot_3.png)
--------
--------


* # Step 4
### ვხურავთ ხარჯის ჩანაწერს და ვინახავთ ცვლილებებს
--------
![](ClaimSetoffScreenShots/Screenshot_4.png)

--------
--------
* # Step 5
### ვხსნით თანხის გაქვითვის ალამს და თავიდან ვუნიშნავთ
--------
![](ClaimSetoffScreenShots/Screenshot_5.png)

--------
--------
* # Step 6
### მონიშვნის შემდეგ გამოვა ფანჯარა და ვავსებთ ველებს, როგორც "სქრინზეა"
--------
![](ClaimSetoffScreenShots/Screenshot_6.png)

--------
--------
* # Step 7
### გადანაწილების მოდულში, პოლისის ნომრით, ოპერაციის დამატებითი ინფორმაციის ველში ვეძებთ ჩანაწერს და ვხსნით რომელიც არ არის წაშლილი, აუცილებელია გამოვაჩინოთ ყველა ჩანაწერი
--------
![](ClaimSetoffScreenShots/Screenshot_7.png)

--------
--------
* # Step 8
### Search ველში ვეძებთ პოლისის ნომრის მიხედვით პოლისს, ძიების შედეგში მოძებნილი ჩანაწერს ვაკლიკებთ ორჯერ
--------
![](ClaimSetoffScreenShots/Screenshot_8.png)


--------
--------
* # Step 9
### გადანაწილებული თანხის ველში ვუთითებთ სასურველი თანხის რაოდენობას და ვინახავთ "Confirm" - ით
--------
![](ClaimSetoffScreenShots/Screenshot_9.png)



