# სადაზღვევო შემთხვევაში პოლისის ნომრის შეცვლა
---
---
* # Step 1
#Core - ის პროგრამიდან შევდივართ მოდულში სადაზღვევო შემთხვევა და განაცხადის ნომრით ვძებნით ზარალს რომელშიც ვცვლით პოლისის  ნომერს


 ### Tools >> Update-დან  Fields-ში ვუაფდეითებთ PolicyBase-ს
---
![](screen1/policy1.png ){: width="100%"}


---
---

* # Step 2
### Core - ის პროგრამიდან შევდივართ მოდულში ზარალის დარეგულირება და იგივენაირად ვუაფდეითებთ პოილისის ნომერს, როგორც სადაზღვევო შემთხვევაში და გვერდით ვუწერთ რისი პოლისიცაა, მაგ: -PR ან -Casco და აშ.
--------
![](screen1/policy3.png ){: width="100%"}




-------
-------
[![](screen1/policy2.png ){: width="100%"}]
--------
--------

* # Step 3
### Core - ის პროგრამიდან შევდივართ მოდულში Motor Insurance Actident ან Non Motor Insurance Actident-ში და თუ Motor Insurance Actident-ია ვუაფდეითებთ Motor insurance package-ს და თუ Non Motor Insurance Actident-ია NOn motor insurance package-ს
--------

[![](screen1/policy4.png ){: width="100%"}
--------
--------
![](screen1/policy5.png ){: width="100%"}
--------
--------

