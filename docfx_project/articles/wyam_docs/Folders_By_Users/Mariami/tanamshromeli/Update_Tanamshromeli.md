# ზარალების მენეჯერების ჩამონათვალში გამოჩენა
---
---
* # Step 1
#ზარალების მენეჯერების ჩამონათვალში ვინმე რომ გამოვაჩინოთ/დავამატოთ, შევდივართ თანამშრომლებში და ვეძებთ ვისაც ვამატებთ.



---
![](tanamshromlebi/tanamshromlebi1.png ){: width="100%"}


---
---

* # Step 2
### ვძებნით ვისაც ვამატებთ და ვხსნით პოზიციას
--------
![](tanamshromlebi/tanamshromlebi2.png ){: width="100%"}


--------
--------

* # Step 3
### პოზიციის ველიდან Ctrl+Shift-ით გადავდივართ ამ ველის დეტალურ ინფორმაციაში, სადაც ვნიშნავთ Is claim officer-ს, ვასეივებთ  და თავიდან ვრთავთ პროგრამას
--------

[![](tanamshromlebi/tanamshromlebi3.png ){: width="100%"}
--------
--------
![](tanamshromlebi/tanamshromlebi4.png){: width="100%"}
--------
--------
* # Step 4
### ვხსნით სადაზღვევო შემთხვევას, New-დან ვირჩევთ Non Motor Insurance Accident და ზარალების მენეჯერების ჩამონათვალში ვეძებთ ვისი დამატებაც გვინდა და ვასეივებთ
-------
-------

![](tanamshromlebi/tanamshromlebi5.png){: width="100%"}
-------
-------
-------
![](tanamshromlebi/tanamshromlebi6.png){: width="100%"}