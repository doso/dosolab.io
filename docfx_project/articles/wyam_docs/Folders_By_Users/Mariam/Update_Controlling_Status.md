

# **პოლისზე კონტროლინგის სტატუსის ცვლილება - დადასტურება**

</br>

გვაწვდიან პოლისის ნომრებს და გვთხოვენ მივანიჭოთ სტატუსი **დადასტურებული**

</br>

**1.** პირველ რიგში ვცვლით პაკეტის ველებს:


- controllingstatus - ვანიჭებთ სტატუსს დადასტურებული, Enum: 6
- controllingreject - ვანიჭებთ False-ს
- controllingapprove - ვანიჭებთ True-ს

</br>

__Update Query__

```sql
update insurancepackage controllingstatus = 6, controllingreject = false, controllingapprove = True where id = (select id from policybase where policynumber = '--პოლისის ნომერი')
```
</br>

**2.** ვამოწმებთ ამ პაკეტებისთვის თუ არსებობს PolicyControllingObject და თუ არ არსებობს ვქმნით/ვაინსერტებთ  შემდეგი ველებით:

**2.1.** თუ არსებობს:

- status - ვანიჭებთ სტატუსს დადასტურებული, Enum: 6

</br>

__Update Query__

```sql
update policycontrolingobject set status = 6 where insurancepackage = (select id from policybase where policynumber = '--პოლისის ნომერი')
```
</br>

**2.2.** თუ არ არსებობს:

- status - ვანიჭებთ სტატუსს დადასტურებული, Enum: 6
- insurancepackage - ვანიჭებთ პაკეტის ID-ს

</br>

__Insert Query__

```sql 
insert into policycontrolingobject (status, insurancepackage) values ('6',  (select id from policybase where policynumber = '--პოლისის ნომერი'));
```
</br>

**3.** პაკეტებს ვაბამთ PolicyControlingObject-ებს

- პაკეტის ველში policycontrolingobject ვა-update-ებთ PolicyControlingObject-ის ID-ს

</br>

__Update Query__
```sql 
update insurancepackage set policycontrolingobject = (select id from policycontrolingobject where insurancepackage = (select id from policybase where policynumber = '--პოლისის ნომერი'))
where id = (select id from policybase where policynumber = '--პოლისის ნომერი');
```