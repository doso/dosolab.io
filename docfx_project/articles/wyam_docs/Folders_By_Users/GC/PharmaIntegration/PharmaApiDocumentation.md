Title: Pharma Integration Documentation
---
# თუ მიმართვა ვადაგასულია:
```json
{
  "result": null, 
  "errorMessage": "მიმართვა ვადაგასულია",
  "errorCode": "ReferralIsExpired",
  "timeGenerated": "2019-03-11T15:05:34.443018Z",
  "isSuccess": false,
  "isFailure": true
}
```

# თუ პირად ნომერზე აქტიური მიმართვები არ არის დაბრუნებდა პასუხი:
```json
{
"result":[],
"errorMessage":null,
"errorCode":null,
"timeGenerated":"2019-03-11T07:19:05.9136479Z",
"isSuccess":true
,"isFailure":false
}
```

სქესის გაგება დამატებულია,

ახალი მიმართვები დაეამტა პირად ნომერზე 12345678911 ,
