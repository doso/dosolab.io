
# ზარალებში აუქციონიში გამარჯვებული პროვაიდერის ინვოისის დამატების ფუნქციონალი.
---
აუქციონის ინვოისები:
---
ზარალის დარეგულირებაში დაემატა ახალი ჩარჩო სახელად "აუქციონის ინვოისები". (იხილეთ სქრინი) 
[![](auction_invoice_files/Screenshot_1.png ){: width="100%"}](auction_invoice_files/Screenshot_1.png )
* # Step 1
როგორ დავამატოთ აუქციონის ინვოისები:
---
- აუქციონის ინვოისებში ვაჭერთ New ღილაკს (სქრინზე ნაჩვენებია როგორც N1).
- გამოვა ახალი ფანჯარა "Auction Invoice" (სქრინზე ნაჩვენებია როგორც N2).
- ვავსებთ შესაბამის ველებს და ვინახავთ.(სქრინზე ნაჩვენებია როგორც N3).
- შენახვის შემდგომ "აუქციონის ინვოისები"_ს  ჩამონათვალში ჩაემატება ჩვენ მიერ შექმნილი ინვოისი.
- შემდგომ შევინახოთ ზარალიც.
- ყოველი ახალი ინვოისის დასამატებლად ეს პროცედურა უნდა გაიაროთ.
[![](auction_invoice_files/Screenshot_2.png ){: width="100%"}](auction_invoice_files/Screenshot_2.png )

* # Step 2
როგორ შევქმნათ "ინვოისი ან კალკულაიცა"_ში ჩანაწერი აუქციონში გამარჯვებული ინვოისის მიხედვით:
---
 - "აუქციონის ინვოისები"_ს ჩამონათვალში ორჯერ დავქლიქოთ აუქციონში გამარჯვებულ ინვოისზე.
 [![](auction_invoice_files/Screenshot_3.png ){: width="100%"}](auction_invoice_files/Screenshot_3.png )
 - გამოვა ფანჯარა შეტყობინებით: "ნამდვილად გსურთ აუქციონის ინვოისის შექმნა?".
 [![](auction_invoice_files/Screenshot_4.png ){: width="100%"}](auction_invoice_files/Screenshot_4.png ) 
 - დათანხმების შემთხვევაში ავტომატურად შეიქმნება ჩანაწერი "ინვოისი ან კალკულაცია"_ში.
 [![](auction_invoice_files/Screenshot_5.png ){: width="100%"}](auction_invoice_files/Screenshot_5.png )
 - თუ იგივე ინვოისზე ისევ ორჯერ დავქლითავთ გამოვა შეტყობინება, რომ "მსგავსი ჩანაწერი უკვე არსებობს. თანხმობის შემთხვევაში ძველი ჩანაწერი წაიშლება და ახალი შეიქმნება.", უარის შემთხვევაში არაფერი შეიცვლება.
 [![](auction_invoice_files/Screenshot_6.png ){: width="100%"}](auction_invoice_files/Screenshot_6.png )
