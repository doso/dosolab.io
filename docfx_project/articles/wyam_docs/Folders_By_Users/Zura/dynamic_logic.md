
# დინამიური ლოგიკა (Dynamic Logic).
---
რა არის დინამიური ლოგიკა?
---
დინამიური ლოგიკა არის ფუნქციონალი Core პროექტში, რომლითაც შეძლებთ ახალი ვერსიის დამატების გარეშე (ანუ ახალი dll_ების დადების გარეშე.) შეცვალო ლოგიკა C#_ის კოდით.

---
როგორ გამოვიყენოთ დინამიური ლოგიკა (DynamicLogic)?
---
* # Step 1
- მოვძებნოთ Dynamic Logic.  "Ctrl + ," საშუალებით.
[![](dynamic_logic_files/Screenshot_1.png ){: width="100%"}](dynamic_logic_filesScreenshot_1.png )
* # Step 2
- შევქმნათ ახალი ჩანაწერი New ღილაკის დაჭრით. სქრინზე ნაჩვენებია ახალი დინამიური ლოგიკის ჩანაწერი.
[![](dynamic_logic_files/Screenshot_2.png ){: width="100%"}](dynamic_logic_files/Screenshot_2.png )
- მარჯვენა კუთხეში Dynamic Logic Group_ში ნაჩვენებია ყველა ის მეთოდი რომლითაც შეძლებ შენი ლოგიკის გამოძახებას.

      - Execute ON Saving
      - Execute On Changed
      - Execute After Construction
      - Execute Only Current Type

    მაგალითად თუ გინდა რომ ობიექტის დასეივების დროს ამოქმედდეს შენი ლოგიკა მაშინ უნდა მონიშნო Execute On Saving.

  რაც შეეხება Is Active და VisibleInWeb bool ტიპის ფროფერთიებს:
    - IsActive_ს ვნიშნავთ იმ შემთხვევაში თუ გვინდა, რომ ჩვენი დინამიური ლოგიკა შევიდეს ძალაში (ამოქმედდეს).
    - VisibleInWeb_ს ვნიშნავთ მაშინ როდესაც გვინდა, რომ დინამიურმა ლოგიკამ იმუშავოს ვების პროექტისთვისაც.

[![](dynamic_logic_files/Screenshot_3.png ){: width="100%"}](dynamic_logic_files/Screenshot_3.png )

* # Step 3
- Type ში უნდა მივუთითოთ ის ობიექტი, რომელი ობიექტის (კლასის) Execute _ის შემდეგაც გვინდა რომ ამუშვდეს ჩვენი ლოგიკა.
- ასევე სავალდებულოა მივუთითოთ დასახელება.

[![](dynamic_logic_files/Screenshot_4.png ){: width="100%"}](dynamic_logic_files/Screenshot_4.png )

* # Step 4
- Property Name ფროფერთიში იმ შემთხვევაში ვირჩევთ მნიშვნელობებს, თი კონკრეტულად რომელიმე ფროფერთის ცვლილების შემდგომ გვინდა Execute _ს აღძვრა.

[![](dynamic_logic_files/Screenshot_5.png ){: width="100%"}](dynamic_logic_files/Screenshot_5.png )

* # Step 5
შენახვის შემდგომ Script_ ში ავტომატურად დაგენერირდება ჩნაწერი, სადაც შეგიძლიათ C# ლოგიკა დაამატოთ.

[![](dynamic_logic_files/Screenshot_6.png ){: width="100%"}](dynamic_logic_files/Screenshot_6.png )

