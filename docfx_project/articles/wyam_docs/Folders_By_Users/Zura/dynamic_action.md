
# დინამიური ლოგიკა (Dynamic Action).
---
რა არის დინამიური ექშენი?
---
დინამიური ექშენი არის ფუნქციონალი Core პროექტში, რომლითაც შეძლებ კოდში ჩარევის გარეშე დაამატო ახალი ღილაკი. ამ ღილაკზე დაჭერით მოხდება თქვენ მიერ გაწერილი ლოგიკა.

---
როგორ გამოვიყენოთ დინამიური ექშენი (DynamicAction)?
---
* # Step 1
- მოვძებნოთ Dynamic Action.  "Ctrl + ," საშუალებით.(იხილე სქრინი)

[![](dynamic_action_files/Screenshot_1.png ){: width="100%"}](dynamic_action_filesScreenshot_1.png )

* # Step 2
- შევქმნათ ახალი ჩანაწერი New ღილაკის დაჭრით. სქრინზე ნაჩვენებია ახალი დინამიური ექშენის ჩანაწერი.(იხილიე სქრინი)

[![](dynamic_action_files/Screenshot_2.png ){: width="100%"}](dynamic_action_files/Screenshot_2.png )

- დასახელებაში ავტომატურად ენიჭება რანდომ OID რომელიც შეგიძლიათ გადააკეთოთ.

[![](dynamic_action_files/Screenshot_3.png ){: width="100%"}](dynamic_action_files/Screenshot_3.png )

- Is Active მონიშნე იმ შემთხვევაში თუ გინდა რომ Dynamic Action ძალაში შევიდეს.* # Step 3
- Image Name ში შეგიძლიათ ჩაწეროთ სურათის დასახელება რომელიც დაედება ღილაკს (სურათი ჩააგდეთ Image ფოლდერში)

* # Step 3
 Selection Depandency Type ფროფერთის მნიშვნელობა
 - Independent: ეს ღილაკი იმუშავებს მიუხედავად იმისა მონიშნული იქნება თუ არა ჩანაწერი
 - Require single object: ღილაკი მოითხოვს რომ ერთი ჩანაწერი იყოს მონიშნული
 - Require multiple objects: ღილაკი იმუშავებს ერთი ან რამდენიმე ჩანაწერის მონიშვნისას
 
[![](dynamic_action_files/Screenshot_8.png ){: width="100%"}](dynamic_action_files/Screenshot_8.png )

* # Step 4
- Target View Type და Target View Nesting _თი შეძლებთ ღილაკი გამოაჩინოთ თქვენ მიერ სასურველ ადგილას.
მაგალიტად, თუ გსურთ რომ ღილაკი მხოლოდ Detail View _ზე ჩანდეს მაშინ Target View Type_ ში აირჩიე Detail View. თუ გინდა რომ List Veiw _ზეც და Detaqil View _ზეც ჩანდეს ამოირჩიე Any.(იხილე სქრინი)

[![](dynamic_action_files/Screenshot_4.png ){: width="100%"}](dynamic_action_files/Screenshot_4.png )

* # Step 5
- Type ში უნდა მივუთითოთ ის ობიექტი, რომელი ობიექტზეც (კლასზეც) გვინდა ამ ღილაკის გამოჩენა.

[![](dynamic_action_files/Screenshot_5.png ){: width="100%"}](dynamic_action_files/Screenshot_5.png )

* # Step 6
- Criteria _ში შეგვიძლია დავწეროთ ლოგიკა რომლის მიხედვითაც სკრიპტში დაწერილი ლოგიკა ამუშავდება ან არა. 
მაგალითად თუ გვინდა რომ მხოლოდ ადმინისტრატორს ჰქონდეს ამ ღილაკის აღზვრის უფლეფა კრიტერიაში ვწერთ ესეთ ლოგიკას:
[CurrentSecuritySystemUser.Roles][[Is Administrative] = True] (იხილე სქრინი)

[![](dynamic_action_files/Screenshot_6.png ){: width="100%"}](dynamic_action_files/Screenshot_6.png )

* # Step 7
შენახვის შემდგომ Script_ ში ავტომატურად დაგენერირდება ჩნაწერი, სადაც შეგიძლიათ C# ლოგიკა დაამატოთ, რომელიც ღილაკზე დაჭერით აღიძვრება.

[![](dynamic_action_files/Screenshot_7.png ){: width="100%"}](dynamic_action_files/Screenshot_7.png )

