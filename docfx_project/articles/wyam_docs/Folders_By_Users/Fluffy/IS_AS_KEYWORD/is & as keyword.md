
<h1 style="text-align:center">ზოგადი</h1>
</br>
</br>
</br>
წარმოვიდგინოთ ზოგადი კლასი ჯართი, რომელსაც აქვს Weight ფროფერთი(გვაწვდის ინფორმაციას ჯართის წონის შესახებ) და ეს კლასი არის მშობელი ყველა იმ კლასისა, რომელიც განეკუთვნება ჯართს. სანამ ჯართის ტიპის კლასებს შემოვიტანთ, შევქმნათ კლასი ჯართი თავისი ზემოხსენებული ფროფერთით:

```csharp
public class Jarti {
    public static int Weight{get;set;}
    }
```
ამის შემდგომ, შევქმნათ კლასები, რომლებიც იქნებიან ჯართის შთამომავლები(განეკუთვნებიან ჯართის კლასს, თუმცა აქვთ დამატებითი თვისებები)


```csharp

public class Radiatori : Jarti
{
    public Color Color{get;set;}
}

public class Axali_Radiatori : Radiatori
{
    public int Price {get;set;}
} 
```
<!--86,156,214--> 

This is *red*{: style="color: red"}

კოდიდან ვხედავთ, რომ შევქმენით კლასი **Radiatori**, რომელსაც აქვს ჯართის ნიშან-თვისებები(ფროფერთი `Weight`) და დამატებითი ნიშან-თვისება <b><span style="color:rgb(156,220,254)">Color</b></span> ფროფერთი, ხოლო კლას <b><span style="color:rgb(78,201,176)">Axali_Radiatori</span></b> აქვს როგორც <b><span style="color:rgb(78,201,176)">Jarti</b></span>-ს, ასევე <b><span style="color:rgb(78,201,176)">Radiatori</b></span>-ს ნიშნები(<b><span style="color:rgb(156,220,254)">Weight</b></span>, <b><span style="color:rgb(156,220,254)">Color</b></span>,ფროფერთები და  <b><span style="color:rgb(78,201,176)">Axali_Radiatori</b></span>-თვის დამახასიათებელი <b><span style="color:rgb(156,220,254)">Price</b></span> ფროფერთი.)

</br>
</br>
</br>

<h1 style="text-align:center">is & as keyword  C# - ში</h1>
</br>
</br>
</br>

<b><span style="color:rgb(86,156,214)">is keyword</b></span>გამოიყენება ობიექტის ტიპების შესადარებლად და დასაბრუნებელი მნიშვნელობა აქვს bool ტიპის. 
განვიხილოთ შემდეგი კოდი უფრო დეტალურად:

```csharp
public void DumpObject(object o)
{
    if(o is int i)
        (i * i).Dump("Int");
    else if(o is string s)
        (s + s).Dump("string");
    else if(o is Jarti j)
        j.Dump("jarti");
}
```

შექმნილია მეთოდი DumpObject,რომელიც პარამეტრად იღებს object-ს(object-ი არის ყველაფრის მშობელი(Class,Int,String და ა.შ),შიგნით კი გვიწერია მარტივი ლოგიკა, რომ თუ გადმოცემული object არის 
int ტიპის, მაშინ მიღებული ციფრი აიყვანოს კვადრატში და გამოიტანოს მიღებული მნიშვნელობა, თუ გადაცემული არგუმენტი არის string, მაშინ შეკრიბოს, ხოლო Jarti-ის შემთხვევაში უბრალოდ გამოიტანოს ეს მნიშვნელობა.

როგორც ვახსენეთ, is keyword -ით ვადარებთ ორ ობიექტს ერთმანეთს(შეესაბამებიან ისინი თუ არა ერთმანეთს) და აბრუნებს bool მნიშვნელობას.

შევქმნათ ახალი ცვლადი, მივანიჭოთ Radiatori-ს ინსტანსი და შევეცადოთ მივწვდეთ მის მეთოდებსა და ველებს(იხ. სურათი #1)

```csharp
var radiatori = new Radiatori():
```
<img src="./img/Screenshot_2.png"></img></br>
<span style="font-size:10px; color:rgb(169,169,169);">სურათი #1</span>
</br>
</br>
დავინახავთ,რომ შეგვიძლია მივწვდეთ ისეთ ველებსა და მეთოდებს, როგორებიცაა: Color,Weight,Equals,GetHashCode და ა.შ

აღსანიშნავია ის, რომ Weight-ზე გვაქვს წვდომა, რადგანაც როგორც უკვე ვთქვით, Radiatori არის ჯართის შვილი, შესაბამისად არის მისი ნიშან-თვისებების მატარებელი, ხოლო Color არის Radiatori -თვის დამახასიათებელი ნიშან-თვისება(რაც არ არის ჯართის შემთხვევაში)







დამატებითი რესურსები:
</br>
წასაკითხი: 

[I'm an inline-style link with title](https://www.google.com "Google's Homepage"){:target="_blank"}


<a href="https://www.geeksforgeeks.org/is-vs-as-operator-keyword-in-c-sharp" target="_blank">GeeksForGeeks</a>
</br>
<a href="https://stackoverflow.com/questions/3786361/difference-between-is-and-as-keyword"  target="_blank">StackOverflow</a>
</br>
<a href="https://www.csharpstar.com/is-vs-as-operators-in-csharp/" target="_blank">CsharpStar</a> 



<iframe width="420" height="315" src="https://www.youtube.com/embed/BGBM5vWiBLo" frameborder="0" allowfullscreen ng-show"showvideo"></iframe>


[![Video](https://www.youtube.com/embed/BGBM5vWiBLo/0.jpg)](https://www.youtube.com/embed/BGBM5vWiBLo)

![](https://www.youtube.com/embed/BGBM5vWiBLo/0)