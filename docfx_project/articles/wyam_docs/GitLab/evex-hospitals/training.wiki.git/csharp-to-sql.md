| SQL | C# |
| ------ | ------ |
| Table | Class |
| Field | Property | 
| Record | Instance | 
| Foreign Key | Property type | 
| Procedure / Function | Method | 
