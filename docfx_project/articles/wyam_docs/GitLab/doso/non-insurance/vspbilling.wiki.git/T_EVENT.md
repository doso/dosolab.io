```SQL
use Reporting

declare @tablename nvarchar(150) = 'T_EVENT'
declare @idColumnName nvarchar(10) = 'ID'
declare @columns nvarchar(max)
SELECT @columns = (STUFF((
SELECT ', ' +
	COLUMN_NAME
FROM
  	INFORMATION_SCHEMA.COLUMNS
WHERE
	TABLE_NAME = @tablename
        FOR XML PATH('')
        ), 1, 2, ''))

declare @maxIdQuery nvarchar(4000)
declare @rowcount int

set @maxIdQuery = N'select @rowcount=count(*) from ' + @tablename
exec sp_executesql @maxIdQuery,
                    N'@rowcount int output', @rowcount output;


DECLARE @TSQL nvarchar(4000)
SET @TSQL = 'SELECT ' + @columns + ' FROM "' + @tablename + '" where ' + @idColumnName + '> ' + CONVERT(varchar, @rowcount)

--EXEC ('SELECT * into T_EVENT FROM OPENQUERY(metra,''' + @TSQL + ''')') 
exec ('insert into ' + @tablename + '(' + @columns + ')
select ' + @columns + '
FROM OPENQUERY(metra,''' + @TSQL + ''')')

```