```sql
use Reporting

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'Contract') drop table Contract
select * into Contract from openquery(metra, 'select * from "Contract"')

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'CUSTOMERSTATUS') drop table CUSTOMERSTATUS
select * into CUSTOMERSTATUS from openquery(metra, 'select * from "CUSTOMERSTATUS"')

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PermissionPolicyUser') drop table PermissionPolicyUser
select * into PermissionPolicyUser from openquery(metra, 'select * from "PermissionPolicyUser"')

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'Balance') drop table Balance
select * into Balance from openquery(metra, 'select * from "Balance"')


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'TokenGroup') drop table TokenGroup
select * into TokenGroup from openquery(metra, 'select * from "TokenGroup"')

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'Token') drop table Token
select * into Token from openquery(metra, 'select * from "Token"')

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PaymantRegime') drop table PaymantRegime
select * into PaymantRegime from openquery(metra, 'select * from "PaymantRegime"')


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'S_MEMBER_CARD') drop table S_MEMBER_CARD
select * into S_MEMBER_CARD from openquery(metra, 'select * from "S_MEMBER_CARD"')

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'L_B_USER') drop table L_B_USER
select * into L_B_USER from openquery(metra, 'select * from "L_B_USER"')

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PaymantWithCashOrCArd') drop table PaymantWithCashOrCArd
select * into PaymantWithCashOrCArd from openquery(metra, 'select * from "PaymantWithCashOrCArd"')

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PaymentInformation') drop table PaymentInformation
select * into PaymentInformation from openquery(metra, 'SELECT
    t6.DOC_DT,
    t1.ID,
    t6.DOc_SUB,
    t6.PAY_VALUE,
    t6.PAY_ID,
    t6.ID_MEMBER,
    t4.DESCRIPTION collate utf8 Description,
    t3.DESCRIPTION collate utf8 as DESCRIPTION1 ,
    t6.ITEM_PRICE,
    t6.ITEM_NUM,
    t6.DOC_USER
    ,t5.DESCRIPTION collate utf8 as DESCRIPTION2
    ,t5.USERNAME
FROM
    T_DOC_INV t6
        LEFT JOIN L_M_MEMBER t1 ON t6.ID_MEMBER = t1.ID
        LEFT JOIN L_ITEM t2 ON t6.ITEM_ID = t2.ID
        LEFT JOIN L_TICKET t3 ON t2.ID_TICKET = t3.ID
        LEFT JOIN L_B_PAY t4 ON t6.PAY_ID = t4.ID
        INNER JOIN L_B_USER t5 ON t5.ID = t6.DOC_USER')


--select * From PaymentInformation




--SELECT [Amount in bracelet],  * FROM [ViewVSP_PersPayments] 
----WHERE [ViewVSP_PersPayments].[Amount in bracelet] =123.45   
--where isnull([Amount in bracelet],0) != 0
--ORDER BY [ViewVSP_PersPayments].[Date] ASC 


--; SELECT  SUM([Amount]) AS [SUM_Amount] FROM ViewVSP_PersPayments 
```