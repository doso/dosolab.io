```csharp
void Main()
{
	var c1 = new Class1();
	var instance = new Class2(c1);

	instance.Dump();

	instance.method1();
	instance.Class1.method1();
}

class Class1
{
	public Class1()
	{
		class1Event += args => args.Dump();
	}

	public Class1(Class1 class1) : this()
	{
		class1Event += args => args.Dump();
	}

	public event Action<string> class1Event;

	public void method1()
	{
		class1Event?.Invoke("method1 called!");
	}

	public void method2()
	{
		class1Event?.Invoke("method2 called!");
	}
}

class Class2 : Class1
{
	public Class1 Class1 { get; }

	public Class2(Class1 _class1) : base(_class1)
	{
		Class1 = _class1;
	}
}
```