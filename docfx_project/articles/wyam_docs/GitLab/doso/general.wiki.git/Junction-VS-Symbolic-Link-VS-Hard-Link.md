https://stackoverflow.com/questions/9042542/what-is-the-difference-between-ntfs-junction-points-and-symbolic-links

![image](uploads/0b86be66a59d5cc59c5eb82febd94b65/image.png)
![image](uploads/4310264948ff9f2408ef8668ee4046d9/image.png)

We are using **Symbolic Links**
Here's a tool for creating links from Explorer - http://schinagl.priv.at/nt/hardlinkshellext/linkshellextension.html

# Removing a Junction / Symbolic Link
https://superuser.com/questions/167076/how-can-i-delete-a-symbolic-link