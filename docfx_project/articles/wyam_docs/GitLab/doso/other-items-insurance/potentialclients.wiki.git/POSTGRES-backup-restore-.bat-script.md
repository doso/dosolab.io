**d:  
cd D:\Program Files\PostgreSQL\10\bin**


*rem pg_dump --dbname=postgresql://postgres:asdfqwer1234%21%40%23%24@127.0.0.1:5432/merged_test > %appdata%\merged_test.dump2  
   rem pg_restore -v --dbname=postgresql://postgres:asdfqwer1234%21%40%23%24@127.0.0.1:5432/postgres2 %appdata%\merged_test.dump2*


*REM -- password environment variable for the session pertaining to PRIME, NOT 148 --*  
**set PGPASSWORD=qwerasdf1234!@#$**

*REM -- backup database with -F c option (binary?) --*  
**pg_dump -h 10.130.130.12 -p 5432 -U postgres -F c -b -v -f D:\Postgres.DB\backup.dmp postgres**

*REM -- password environment variable for the session for 148 --*  
**set PGPASSWORD=asdfqwer1234!@#$**

*REM -- drop and recreate database --*  
**dropdb -h 192.168.250.148 -p 5432 -U postgres prime_restored  
createdb -h 192.168.250.148 -p 5432 -U postgres prime_restored**

*REM -- restore --*  
**pg_restore -h 192.168.250.148 -p 5432 -U postgres -d prime_restored -v D:\Postgres.DB\backup.dmp**