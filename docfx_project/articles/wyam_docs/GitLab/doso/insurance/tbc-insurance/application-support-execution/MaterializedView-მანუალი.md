


პროგრამაში დამატებულია ცხრილი რომელიც გვაწვდის ინფორმაციას მატერიალიზებულ ვიუებში როდის მოხდა იფორმაციის დარეფრეშება.


ამჯერად ფუნქცია არეფრეშებს შემდეგ მატერიალიზებულ ვიუებს:


1. currencyratesviewm
2. scheduleredistributionsviewm
3. tbcportfolioviewm
4. sme_policies_for_portfolio_report
5. commitmentscheduleredistributionsviewm

materialviewinfo ცხრილს აქვს ველი refreshinterval რომელშიც წერია რა ინტერვალით რეფრეშდება.


```sql
select * from materialviewinfo
```


