1. თავდაპირველად პრობრამაში ვეძებთ და ვხსნით Do So Sms Schedule - ს. 

![ვხსნით_One_Time_Sms_-_ს](uploads/37dc7139933cfc7bb77b489d1c656b39/ვხსნით_One_Time_Sms_-_ს.png)

2. ვეძებთ და ვხსნით სქედულს დასახელებით : One Time Sms.

![ვხსნით_One_Time_Sms_-_ს](uploads/c457af33285a97838cd5d1e52caa5aaa/ვხსნით_One_Time_Sms_-_ს.png)

3. სქედულის ლოგიკა განისაზღვრება Objects Query - ში არსებული სელექთით და რეზულტატის ნახვა შესაძლებელია Show Object - ის ღილაკის მეშვეობით, სადაც გენერირდება ინფორმაცია რომელ პოლისზე, რომელ ნომერზე და რა ტექსტი იგზავნება.

![Show_Objects_-_ის_ჩვენება](uploads/b900036b830fb34d49eac1c97c26578b/Show_Objects_-_ის_ჩვენება.png)

4. ასევე შესაძლებელია გადამოწმება გამოგზავნილ ფაილში რა დავალიანება მითითებული პოლისი და რას აბრუნებს სქედულის სელექთი. როგორც სქრინებიდან ჩანს მაგალითად პოლისზე K/TMTPL/R/001151/19 გამოგზავნილ ფაილში და Objects Query - ის მონაცემებში ერთი და იგივე დავალიანებას. 

![დავალიანება_ფაილის_მიხედვით](uploads/b5bd260335f258705b9507f93e43fa11/დავალიანება_ფაილის_მიხედვით.png) 

![Show_Object_-_ით_გასაგზავნი_სმს_-_ების_შემოწმება_1](uploads/32d17b1bf5e6da235e3e28bb157113a9/Show_Object_-_ით_გასაგზავნი_სმს_-_ების_შემოწმება_1.png)

5. შესაძლებელია  Objects Query - ის მიერ დაბრუნებული მონაცემებიდან ზოგი პოლისი იყოს დუბლირებული, ამის მიზეზე ის არის რომ დამსზღვევს შეიძლება ჰქონდეს მითითებული რამდენიმე საკონტაქტო ნომერი.

![პოლისის_დუბლირება_2_ნომრის_გამო](uploads/1a7f43d24af1d33f1fe288079d44fb96/პოლისის_დუბლირება_2_ნომრის_გამო.png)

6. მას შემდეგ რაც დავრწმუნდებით, რომ სასურველ პოლისებზე ვაგზავნით სმს - ებს. სქედულს ვურთავთ IsActive - ს და ვინახავთ.

![ვუნიშნავთ_Isactive_-__ს](uploads/aebdc6ba126516831b76e8430861aa96/ვუნიშნავთ_Isactive_-__ს.png)

7. შემდეგ სმს ების დასაგენერირებლად ვააქტიურებთ GenerateSms ღილაკს.

![სმს_ების_დაგენერირება__Generate_Sms_](uploads/f9e51baefb939e1abb03977c9aca9782/სმს_ების_დაგენერირება__Generate_Sms_.png)

8, იმის გადასამოწმებლად გაიგზავნა თუ არა სმს - ი SmsCollection - ის ტაბში Sending Date - ს ვფილტრავთ Today - ით რადგან დაიფილტროს დღევანდელი გაგზავნილი სმს - ები. დაფილტვრის შემდეგ რაც გამოჩნდება სვეტი IsSent უნდა ჰქონდეს True მნიშვნელობის. ეს ისმას ნიშნავს რომ სმს ები გაგზავნილია.

![Sms_Collection_-_ში_Sending_Date_-_ში_ვირჩევთ_Today_-_ს](uploads/04d7b85d51d73fef34808bcee0decb44/Sms_Collection_-_ში_Sending_Date_-_ში_ვირჩევთ_Today_-_ს.png)

![IsSent_უნდა_იყოს_მონიშნული](uploads/e84b23f373d96ff9804565e8317d019e/IsSent_უნდა_იყოს_მონიშნული.png)

9. მას შემდეგ რაც დავრწმუნდებით რომ ყველა სმს გაგზავნილია სქედულს IsActive - ს ღილაკს ვუთიშავთ და ვინახავთ. 

![ვუნიშნავთ_Isactive_-__ს](uploads/835487a6633d7f3fce7c89e84407c61e/ვუნიშნავთ_Isactive_-__ს.png)

10, რადგან სმს ები უკვე გაგზავნილია, გამოგზავნილ პოლისებზე უნდა დაინსერტდეს გაუქმების მოთხოვნა და წერილი. ამისთვის დაგვჭირდება გამოგზავნილი პოლისების ID - ები, რითაც შემდეგი სელექთით დავაბრუნებთ:

```sql

select id
 from policybase
 where policynumber in ('K/TMTPL/R/001165/19',
                        'PP/1000271/19',
                        'PP/1000041/19',
                        'K/MO/R/075342/19',
                        'K/TMTPL/R/001151/19',
                        'MO/106327/19-2',
                        'K/MO/R/069733/19',
                        'MO/106327/19-1',
                        'PP/1000306/19',
                        'MO1002121/19',
                        'PP/1017049/19',
                        'PP/1000764/19',
                        'PP/1003699/19',
                        'PP/1001469/19')
``` 

11. შედეგი რაც დაგვიბრუნდება გაუქმების მოთხოვნის ინსერტის Where - ის ნაწილში უნდა მიუთითოთ ამ პოლისების ID - ები და გაუშვათ ინსერტი: 

```sql
--გაუქმების მოთხოვნის ინსერტი
insert into cancelationrequest (createdon, savedon, createdby, policybase, requestdate, cancellationdate,  unearnedpremium, type, ispromptednewpolicy, iscancelled)
  select --pp.policynumber,
    s.instalmentdate::date  + INTERVAL '1day' as createdon, now() as savedon, '04e2535a-5f75-4596-8410-1ea9d3e1e496' createdby, pp.id policybase, s.instalmentdate::date  + INTERVAL '1day' requestdate, s.instalmentdate::date  + INTERVAL '31day' cancellationdate,
    pp.grosswrittenpremium -
    (select  CASE WHEN (SELECT ExpireComment
                      FROM CustomClass4CancelPolicy
                      WHERE ID = p.CustomClass4CancelPolicy
                            AND ExpireComment = 15
                      LIMIT 1) IS NOT NULL  AND p.CancellationDate <= s.instalmentdate::date  + INTERVAL '31day'
             THEN (p.GrossWrittenPremium )
           ELSE CASE WHEN p.EffectiveDate < s.instalmentdate::date  + INTERVAL '31day'
             THEN (DATEDIFF('day', p.EffectiveDate, least(COALESCE(p.CancellationDate
                     + INTERVAL '1 day', p.ToDate + INTERVAL '1 day'),
                                  DATEADD('day', 1, s.instalmentdate::date  + INTERVAL '31day'))) * 1.0)
                  / DATEDIFF('day', p.EffectiveDate - interval '1 day', p.ToDate)
                  * p.GrossWrittenPremium
                ELSE 0  END   END  from policybase p where id = pp.id
) unearnedpremium,
    '1' as type, FALSE as ispromotednewpolicy, FALSE as iscancelled
      from policybase pp
      join insurancepackage ip on ip.id = pp.id
      JOIN schedule s on s.insurancepolicy = pp.id AND s.isactive AND s.expiredon is NULL AND s.ordinarynumber > 1 AND s.instalmentdate::date =
      (select max(ss.instalmentdate::date)  from schedule ss where ss.insurancepolicy = pp.id and ss.isactive and ss.expiredon isnull and ss.instalmentdate < now()) -- ???? ???????????
      --now()::date - interval '1day'
where pp.cancellationdate is  null and
      pp.expiredon is null --and pp.visibleinweb
      and pp.license in (1, 36, 35, 23, 26, 53)
 and s.currentdebt >0 and s.currentdebt / s.instalmentamount > 0.1
 and pp.id in (785466, 873912,762855,766024,810138,1052604,1052607,735855,736315,736385,764339,770917,808646,1053621) -- აქ ჩავაგდებთ გამოგზავნილი პოლისების ID ებს
;
```

12. შემდეგ ვაინსერტებთ წერილს:

```sql
 -- წერილის ინსერტი
insert into letter (createdon, savedon, createdby, letternumber, lettertext, lettergenerator, policybase, contrahentbase,  debt,  currency, generatedon, sentdate, receivedate, lettersendingtype, letterstatus, lettergeneratorschedule, sms, cancelationrequest)
  select --pp.policynumber,
    now() as createdon, now() as savedon, '04e2535a-5f75-4596-8410-1ea9d3e1e496' createdby, 'Custom Letter' letternumber,
    (with cte as (select  max(d2.id) smsid from dososms ds
     join dosomessagebase d2 on ds.id = d2.id and ds.dososmsschedule = 196 and d2.issent = true
     where d2.objectkey = pp.id::varchar(50))
     select sms.smstext from cte
     join dososms sms on sms.id = cte.smsid
    ) lettertext,
    case when pp.license in (1, 36, 26) then 14 when pp.license = 35 then 16 when pp.license = 23 then 15 end as lettergenerator,
    pp.id policybase, pp.contrahent contrahentbase, s.currentdebt AS debt,
    pp.grosswrittenpremiumcurrency currency,
    now()::date generatedon, now()::DATE sentdate,  s.instalmentdate::date  + INTERVAL '1day' receivedate,
    '3' lettersendingtype, '2' letterstatus,
    case when pp.license in (1, 36, 26) then 84 when pp.license = 35 then 86 when pp.license = 23 then 85 end as lettergeneratorschedule,
    (select max(ds.id) from dososms ds
     join dosomessagebase d2 on ds.id = d2.id and ds.dososmsschedule = 196 and d2.issent = true
     where d2.objectkey = pp.id::varchar(50)
    ) as sms,
    (select max(cr.id) from cancelationrequest cr
     where  pp.id = cr.policybase and cr.createdby = '04e2535a-5f75-4596-8410-1ea9d3e1e496' and cr.type = 1) as cancelationrequest
      from policybase pp
      join insurancepackage ip on ip.id = pp.id
      JOIN schedule s on s.insurancepolicy = pp.id AND s.isactive AND s.expiredon is NULL AND s.ordinarynumber > 1 AND s.instalmentdate::date =
      (select max(ss.instalmentdate::date)  from schedule ss where ss.insurancepolicy = pp.id and ss.isactive and ss.expiredon isnull and ss.instalmentdate < now()) -- ???? ???????????
      --now()::date - interval '1day'
where pp.cancellationdate is  null and pp.expiredon is null --and pp.visibleinweb
  and pp.license in (1, 36, 35, 23, 26, 53)
 and s.currentdebt > 0 and s.currentdebt / s.instalmentamount > 0.1
and pp.id in (785466, 873912,762855,766024,810138,1052604,1052607,735855,736315,736385,764339,770917,808646,1053621) -- აქ ჩავაგდებთ გამოგზავნილი პოლისების ID ებს
;
```