რეპორტში Digital Offloading არის “Sql query” რომლის ინფორმაციის საფუძველზე ხდება template ექსელის ფაილის მიხედვით მონაცემების კალკულირება. “Sql query” - ის შედეგი გენერირდება template – ის Data შიტზე, Templates გადმოწერა იხილეთ სქრინზე: 

![Screenshot_1](uploads/2ae4c5219e7615d837facf2a42eb77d3/Screenshot_1.png) 

Template – ს data შიტზე როგორც სქრინშოთზეა ნაჩვენები მონიშნულ ველებზე უნდა გენერირდებოდეს ის ფორმულები რაც issue #1431 - შია მოწერილი, იხილეთ სქრინი ქვემოთ.

![Screenshot_2](uploads/d13542225fd4b175005ba932f9c037e1/Screenshot_2.png) 

გამოგზავნილი ფორმულები:

![Screenshot_3](uploads/f21504e15b8f8ebca4778134a5ecd6fe/Screenshot_3.png) 

რადგან Sql query - დან წამოღბული დატა შეიძლება სხვადასხვა რაოდენობის იყოს ამიტომ გამოგზავნილი ფორმულები უნდა გადავწეროთ sql სკრიპტში : 

ამჟამიდელი ლოგიკა:
Sql Query - ში AB, AC, AD და AE ველებისთვის არის შემდეგი: 

```sql
,case when (p.effectivedate::date = p.cancellationdate::date + interval '1day') or
 case when coalesce(p.CancellationDate, p.todate) = p.fromdate then 'ანულირებული'
        when coalesce(p.CancellationDate, p.todate) > p.fromdate and p.istotalloss = True then 'ტოტალი'
        when  p.CancellationDate is not null then 'გაუქმებული'
        when  p.todate < now()::date  then 'ვადაგასული'
        when  p.todate > now() and p.cancellationdate is null then 'აქტიური'
      else ''
      end = 'ანულირებული' then true
  else false
              end AB,
  case
  when ch.name = 'Retail' and coalesce(ch2.name, 'NOTWissol') != 'Wissol' and coalesce(digp.digitalplatform, 1) != 18 then true
  when ch1.name = 'Bancassurance(UW)' and  coalesce(digp.digitalplatform, 1) != 18 then true
  when ch.name = 'Bancassurance' and ch2.name = 'Retail' and  coalesce(digp.digitalplatform, 1) != 18 then true
  when ch.name = 'Bancassurance' and ch1.name = 'Crystal' and  coalesce(digp.digitalplatform, 1) != 18 then true
  when ch.name = 'Bancassurance' and ch1.name = 'GeorgianCredit' and  coalesce(digp.digitalplatform, 1) != 18 then true
else false
              end AC
,case
    when ch.name = 'Bancassurance' and ch2.name = 'Retail' and  coalesce(digp.digitalplatform, 1) != 18 then true
    when ch.name = 'Bancassurance' and ch1.name = 'Crystal' and  coalesce(digp.digitalplatform, 1) != 18 then true
    when ch.name = 'Bancassurance' and ch1.name = 'GeorgianCredit' and  coalesce(digp.digitalplatform, 1) != 18 then true
else false
             end  AD
,case
     when ch.name = 'Retail' and coalesce(ch2.name, 'NOTWissol') != 'Wissol' and coalesce(digp.digitalplatform, 1) != 18 then true
  else false
             end AE
```

რეპორტის Sql query – ს ეს ნაწილი უნდა შეიცვალოს შემდეგით, Query - თ. ასევე განახლებულ query - ში მიწერილი მაქვს რომელი ველი რომელი ფორმულის მიხედვით არის აწყობილი ეს იქნება AB, AC, AD თუ AE - ს
განახლებული ველის ლოგიკა.

განახლებული ლოგიკა: 
```sql
-- Sql - ის ეს სკრიპტი იდენტურია იშუზე (#1431) გამოგზავნილი ექსელის ფაილის AB სვეტის ფორმულის: =IF(OR(J2=I2,H2="ანულირებული"),TRUE,IF(AND(L2="Criticalillness",I2<=1),TRUE,FALSE))

  ,case when (p.effectivedate::date = p.cancellationdate::date + interval '1day') or
 case when coalesce(p.CancellationDate, p.todate) = p.fromdate then 'ანულირებული'
        when coalesce(p.CancellationDate, p.todate) > p.fromdate and p.istotalloss = True then 'ტოტალი'
        when  p.CancellationDate is not null then 'გაუქმებული'
        when  p.todate < now()::date  then 'ვადაგასული'
        when  p.todate > now() and p.cancellationdate is null then 'აქტიური'
      else ''
      end = 'ანულირებული' then true
      WHEN li.name = 'CriticalIllness' and pp.cancelledgrosspremium * po.GWPCurrency <= 1 then true
      WHEN pp.GrossWrittenPremium * po.GWPCurrency  =
           coalesce(pp.cancelledgrosspremium * po.GWPCurrency, 0) then true

  else false end AB,

 -- Sql - ის ეს სკრიპტი იდენტურია იშუზე (#1431) გამოგზავნილი ექსელის ფაილის AC სვეტის ფორმულის: =IF(AND(M2="Retail",O2<>"Wissol",R2<>18),TRUE,IF(AND(N2="Bancassurance(UW)",R2<>18),TRUE,IF(AND(M2="Bancassurance",N2<>"SME",R2<>18,O2<>"SME"),TRUE,FALSE)))

  case
  when ch.name = 'Retail' and coalesce(ch2.name, 'NOTWissol') != 'Wissol' and coalesce(digp.digitalplatform, 1) != 18 then true
  when ch1.name = 'Bancassurance(UW)' and  coalesce(digp.digitalplatform, 1) != 18 then true
  when ch.name = 'Bancassurance' and (coalesce(ch1.name, 'NOSME') != 'SME' or  coalesce(ch1.name, 'NOSME') != 'SME')
           and  coalesce(digp.digitalplatform, 1) != 18 then true

else false
end AC

-- Sql - ის ეს სკრიპტი იდენტურია იშუზე (#1431) გამოგზავნილი ექსელის ფაილის AD სვეტის ფორმულის: =IF(AND(M2="Bancassurance",N2<>"SME",R2<>18,O2<>"SME"),TRUE,FALSE)

,case when ch.name = 'Bancassurance' and (coalesce(ch1.name, 'NOSME') != 'SME' or  coalesce(ch1.name, 'NOSME') != 'SME')
           and  coalesce(digp.digitalplatform, 1) != 18 then true
else false
end AD

-- Sql - ის ეს სკრიპტი იდენტურია იშუზე (#1431) გამოგზავნილი ექსელის ფაილის AE სვეტის ფორმულის: =IF(AND(M2="Retail",O2<>"Wissol",R2<>18),TRUE,FALSE)

,case when ch.name = 'Retail' and coalesce(ch2.name, 'NOTWissol') != 'Wissol' and coalesce(digp.digitalplatform, 1) != 18 then true
  else false
end AE
```

იშუს ლინკი

https://gitlab.com/doso/insurance/tbc-insurance/applicationsupportgroup/application-support/issues/1431

