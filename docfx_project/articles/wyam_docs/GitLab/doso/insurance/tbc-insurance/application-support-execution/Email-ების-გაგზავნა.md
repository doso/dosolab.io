Email-ების გაგზავნა ხდება Doso report Schedule-დან 

*  subject-ის მიხედვით შეგვიძლია მოვძებნოთ კონკრეტულად რომელი სქედული უზრუნველყოფს ამ მეილის გაგზავნას.

![image](uploads/5c702253a02253e98268fa914a4283ef/image.png)



და შემდეგ Message to -ში რაც წერია იმ ლოგიკის მიხედვით ხდება Email-ის გაგზავნა, თუ რამის დამატება ან ცვლილება იქნება საჭირო აქ უნდა შევცვალოთ.

*  ![image](uploads/d370aa0527cbf27f17f40be683d586ea/image.png)



მაგალითისთვის განციხილოთ ერთ-ერთი შემთხვევა:

*  აღნიშნული სქედული იგზავნება `სადაზღვევო შემთხვევების` მიხედვით (Target Object Type-ში მითითებულია - 'სადაზღვევო შემთხვევა'

*  MessageTo-ში წერია შემდეგი [ClaimOfficer.Employee.EMail], აქედან შეგვიძლია ამოვიკითხოთ შემდეგი: ძირითად ობიექტს, ანუ ამ შემთხვევაში 'სადაზღვევო შემთხვევას', აქვ ველი - ClaimOfficer, ამ ველიდან შეგვიძლია გავიდეთ ამორჩეული ოფიცერის 
შესაბამის, თანამშრომლის ჩანაწერზე საიდანაც აიღებს Email ველში მითითებულ მონაცემს და მასზე მოხდება გაგზავნა.

 
*  Message To (და მსგავსი კრიტერიები) კონკრეტული ჩანაწერისთვის რა მნიშვნელობას დააბრუნდებს შეგვიძლია გადავამოწმოთ შემდეგნაირად:
ველის გასწვრივ მარჯვენა ბოლოში არის სამი წერტილი, მასზე დაჭრით გამოსული ფანჯრის ქვედა ნაწილში არის ღილაკი Validate, 

Validate ღილაკზე დაჭერით გამოვა სადაზღვევო შემთხვევის List (იმიტომ რომ ძირითადი ობიექტი არის სადაზღვევო შემთხვევები)
ამ List-ში ჩანაწერის არჩევით და მაუსის ორჯერ დაჭერით გამოვა პატარა ფანჯარა, სადაც გვაჩვენებს კონკრეტული შემთხვევბისთვის რა email-ზე მოხდება გაგზავნა

![image](uploads/3beff3dc63437f00d018a5adb7604aeb/image.png)
