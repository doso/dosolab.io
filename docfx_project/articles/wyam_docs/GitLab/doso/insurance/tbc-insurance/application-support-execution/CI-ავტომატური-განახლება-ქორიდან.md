1. პროგრამაში ვეძებთ **Policy Renewal Schedule** და ვხსნით **"კრიტიკული დაავადებებსი პოლისების ავტომატურ განახლებას" **

![Screenshot_1](uploads/bbf7c20ddfb7f4a00a025428571f7922/Screenshot_1.png)

2. **Show Object** ღილაკის საშუალებით ვამოწმებთ რომელი პოლისების განახლებები გენერირდება სქედულის გაშვებისას.

![Screenshot_2](uploads/ec7bb117eea38cf57b9741b58cc2d5dd/Screenshot_2.png)

![Screenshot_3](uploads/504774b9a3a7922af2fce155431c6e50/Screenshot_3.png)

3. შემდეგ ვაჭერთ ღილაკს **"რენევალების დაგენერირება"** - ს

![Screenshot_4](uploads/642af6387b89cd1c382b989d6837e462/Screenshot_4.png)

4. რენევალების დაგენერირების შემდეგ **Renewal Collection** - ის ტაბში ვადამოსულ პოლისის ველში გამოგვიჩნდება პოლისი რომლის განახლებაც გვინდა, და დავაჭერთ შემდეგ **პოლისების განახლება** - ს, რომელიც შექმნის განახლებულ პოლისს.

![Screenshot_5](uploads/bce732ce85f5cb21b04255052ccd2be9/Screenshot_5.png)

5. პროგრამაში ვეძებთ **Validate Policy Schedule** - ს აქაც **Show Object** - ის მეშვეობით ვამოწმებთ რომელი განახლებული პოლისების ვალიდაციას მოახდენს სქედული. შემდეგ ვაჭერთ **პოლისების ვალიდაციის** ღილაკს.

![Screenshot_6](uploads/071bf91cdf2169e86f877f593aef78fd/Screenshot_6.png)

![Screenshot_7](uploads/6c15239d81ec1d3d490609731918085f/Screenshot_7.png)

