* In the solution folder place the GetterSetter2 folder with GetterSetter2.Fody.dll in it
* In each .csproj file, where GetterSetter2 should be included, under all the References, add the following:
```xml
<WeaverFiles Include="$(SolutionDir)GetterSetter2\GetterSetter2.Fody.dll" />
```
![image](uploads/b37719eb095c6be5b7c75d092358d27f/image.png)
* Update Fody nuget package to the latest version (6.0.3 at the time of writing this)
* Under the project, there will be a FodyWeavers.xml file that needs to describe the GetterSetter2 weaver, as follows:
```xml
<?xml version="1.0" encoding="utf-8" ?>
<Weavers>
  <GetterSetter2 />
</Weavers>
```
