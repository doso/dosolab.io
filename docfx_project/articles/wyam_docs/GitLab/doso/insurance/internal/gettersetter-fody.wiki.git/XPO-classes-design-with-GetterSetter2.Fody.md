### Guideline for creating XPO persistent objects
```csharp
public class XpoPersistentObject : XPObject
{
    // Create XPO objects with **protected** instead of **public** constructors
    protected XpoPersistentObject(Session session) : base(session) { }

    // Create public persistent properties first with **private** setters
    public [AnyType] PersistentPropertyWithPrivateSetter { get; private set; }

    // Create public persistent properties with **protected** setters, if private is too restrictive
    public [AnyType] PersistentPropertyWithProtectedSetter { get; protected set; }

    // Use a static Create method to create an instance of the object and initialize all its values
    public static [ReturnType] Create([AnyType] value1, [AnyType] value2, UnitOfWork session) => new XpoPersistentObject(session)
    {
        PersistentPropertyWithPrivateSetter = value1,
        PersistentPropertyWithProtectedSetter = value2
    };

    // Ignore properties by applying an attribute that you first have to describe in your project
    public class GetterSetterIgnoreWeavingAttribute : Attribute { }

    [GetterSetterIgnoreWeavingAttribute]
    public [AnyType] PropertyToIgnoreFromGetterSetter { get; private set; }

    // Use an instance method for updating values, as needed
    public void DoSomeOperation([AnyType] value1)
    {
        PersistentPropertyWithPrivateSetter = value1;
    }
}
```