* Zoho Mail
* Slack
* Gitlab Account
* Clockify
* Issue Manager
* Git Extension
* The Great Suspender

# Zoho Mail
საჭიროა, რომ შეიქმნას სამუშაო მეილი 

# Slack
მეილის შექმნის შემდგომ, ახალი მომხმარებელი უნდა მოვიწვიოთ **DoSo WorkSpace**-ში და შემდგომ შესაბამის ჩანელებში: **doso**, **general**

# Gitlab Account

ვუქმნით მომხმარებელს  **Gitlab**-ის account-ს, რათა ქონდეს წვდომა არსებულ პროექტებსა და Issue-ბზე. გასათვალისწინებელია, რომ უნდა მოვიწვიოთ `sample.sample@doso.ge` მეილით და `Username` შესაბამისი პრინციპით : `@firstname.lastname` (ისე, როგორც მის სამუშაო მეილშია)

# Clockify

ახალ მომხმარებელს ვუქმნით Clockify-ს account-ს, რომ შევძლოთ მისი დროისა და აქტივობების აღრიცხვა. (რა საკითხს რამდენი დრო დაუთმო/რაზე მუშაობდა და ა.შ.)

# Issue Manager

Issue Manager-ში ვაინტეგრირებთ მის მონაცემებს `GitlabUserID`, `GitlabApiKey`-სა და `ClockifyApiKey`-ს მეშვეობით, რათა შეძლოს საკუთარი Issue-ების ნახვა და შესაბამის Issue-ზე Shortcut-ებით შეძლოს Issue-ს დაწყება/გაჩერება.

* `GitlabUserID` - **Settings > Profile**
![gitlabUserID](uploads/7cea267578a84bfe6c6f0e28106234bf/gitlabUserID.png)

* `GitlabApiKey` - **Settings > Access Tokens**  და ვმოქმედებთ შემდეგნაირად: 
![gitlabapikey](uploads/4971011d4f18fe0b408185dbe6f54735/gitlabapikey.png)
![gitlabapikey2](uploads/965b6cae81bf3fea1cbc19aa4246fffd/gitlabapikey2.png)

* `ClockifyApiKey` - **Profile Settings > API**:

![clockifyapikey](uploads/c1bf1f77e7b4dec3bd08b65720f1b286/clockifyapikey.png)


# Git Extension (Developers Only)

საჭიროა, რომ დეველოპერმა წამოიღოს ყველა არსებული რეპოზიტორი, რომელშიც ჩვენ გვიწევს მუშაობა. ამისათვის, საჭიროა გვქონდეს `GitlabAccessToken`-ი.
რეპოზიტორები, რომლებიც დეველოპერმა უნდა წამოიღოს:

* solution-files
* doso-core-insurance
* general-modules
* doso-compiled-binaries
* tbcinsurance
* primeinsurance

**პირველ ჯერზე კლონირებისას, მოგვთხოვს, რომ შევიყვანოთ Username & Password-ი. Username-ში ვწერთ `Gitlab`-ზე არსებულ username-ს, ხოლო პაროლში ვწერთ გიტლაბის მიერ დაგენერირებულ Access Token-ს.**

იმისათვის, რომ დეველოპერმა შეძლოს ეფექტურად მუშაობა და source-control-ი  საჭიროა, რომ დავაკონფიგურიროთ `GitMultiRepositoryManager`-ის (აპლიკაცია, რომელიც ექნება დესკტოპზე გამოტანილი) კონფიგურაცია. ამისათვის შევდივართ **Run > %appdata% > `GitMultiRepositoryManagerConfiguration.config`**

```xml
<GitMultiRepositoryManagerConfiguration>
  <RootFolderPath>D:\Users\giorgi.tsouvaltzis\repos\InsCore</RootFolderPath>
  <RepositoryFoldersList>
    <String>solution-files</String>
    <String>doso-core-insurance</String>
    <String>general-modules</String>
    <String>doso-compiled-binaries</String>
    <String>tbcinsurance</String>
    <String>primeinsurance</String>
    <String>internal-support</String>
  </RepositoryFoldersList>
  <GitUsername>giorgi.tsouvaltzis</GitUsername>
  <Email>giorgi.tsouvaltzis</Email>
  <UserDisplayName>giorgi.tsouvaltzis</UserDisplayName>
  <GitPassword>MEzbnmDaczCSyVJtpxcZ</GitPassword>
</GitMultiRepositoryManagerConfiguration>
```

* RootFolderPath-ში ვწერთ მისამართს, სადაც თავმოყრილია დეველოპერის მიერ დაკლონილი რეპოზიტორები
* String -ში (თუ დაკლონილი რეპოზიტორი არ არსებობს) ვამატებთ რეპოზიტორის სახელს, რათა `GitMultiRepositoryManager`-მა აწარმოოს მონიტორინგი. დამატება ხდება შემდგომნაირად:
```xml
<String>რეპოს-სახელი</String>
```
* GitUserName - გიტლაბის username
* GitPassword - გიტლაბის Access Token



# The Great Suspender

* The Great Suspender - chrome extension-ი, რომელიც ავტომატურად აჩერებს პროცესს არააქტიური Tab-ების




