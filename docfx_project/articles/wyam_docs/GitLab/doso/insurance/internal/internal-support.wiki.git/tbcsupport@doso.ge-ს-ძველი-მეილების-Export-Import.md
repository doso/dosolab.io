tbcsupport@doso.ge -ს დაექსპორტებული მეილები, 

![image](uploads/9c3b70f83fc481268fcebceeefe4a3cc/image.png)

რომლებიც ხელმისაწვდომია შემდეგ რეპოში

https://gitlab.com/doso/insurance/internal/old-tbc-support-mails


**როგორ მოვახდინო დაექსპორტებული მეილების აღდგენა?**

საჭიროა შეხვიდე tbcsupport@doso.ge -ს მეილზე 

გადადიხარ Setting -ებში, 

![image](uploads/915d177cf95a313a27b42a05ee480435/image.png)

სადაც იყენებ Import History -ს

რეპოში არსებულ ზიპ ფაილს უთითებ იმპორტის ველში

![image](uploads/068dc1c35d66ae6d9996d1d954b021fa/image.png)

რის შემდეგაც ყველა მეილი რომელიც დაზიპულია აღდგება.