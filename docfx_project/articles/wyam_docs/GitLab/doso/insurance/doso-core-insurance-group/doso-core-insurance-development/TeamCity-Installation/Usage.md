იმისათვის, რომ შევძლოთ ალურესთან მუშაობა საჭიროა, რომ `Project`-ში დაყენებული გვქონდეს **[NUnit.Allure](https://www.nuget.org/packages/NUnit.Allure/)**. ან **Visual Studio**-ს **Package Manager**-ის კონსოლში გავუშვათ შემდეგი ბრძანება:
```csharp
Install-Package NUnit.Allure
```

ამის შემდგომ, იმ ტესტების პროექტში, რომელშიც გვექნება **NUnit.Allure**-ს პაკეტი დაყენებული მოვძებნოთ `allureConfig.json` ფაილი, შევიდეთ მის **Properties**-ში (`Alt+Enter`,ან `Right Click > Properties`)

და **Advanced** ტაბში შევუცვალოთ **Copy to Output Directory**-ს მნიშვნელობა **Copy if newer**-ზე.


ამის შემდგომ, შეგვიძლია პროექტში დავამატოთ კლასი და იმისათვის, რომ შევძლოთ `Allure`-ს გამოყენება საჭიროა, რომ ამ კლასს დაედოს ატრიბუტი `[AllureNUnit]`.

>Allure Test Sample
```csharp
[TestFixture]
[AllureNUnit]
[AllureDisplayIgnored]
class TestClass
{
    [Test(Description = "XXX")]
    [AllureTag("Regression")]
    [AllureSeverity(SeverityLevel.critical)]
    [AllureIssue("ISSUE-1")]
    [AllureTms("TMS-12")]
    [AllureOwner("User")]
    [AllureSuite("PassedSuite")]
    [AllureSubSuite("NoAssert")]
    public void TestSample()
    {
        Console.WriteLine(DateTime.Now);
    }
}
```

მისი ვიზუალური მხარე:
![allure](/uploads/ecf278ea60e2b51157d9e500a13cffc2/allure.png)


`Allure` გვაძლევს ისეთ საშუალებებს, როგორიცაა :

* მომხმარებლისთვის ტესტ ქეისების შედეგების ვიზუალიზაცია
* ტესტ ქეისების მიბმა Issue-ზე
* ტესტ ქეისების დაჯგუფება (იგულისხმება ერთი სახელწოდების ქვეშ გაერთიანება რამდენიმე ტესტ ქეისის)
* მომხმარებლისთვის უფრო მარტივია ტესტ ქეისებით გაერკვიოს მიმდინარე ლოგიკაში ან/და შეძლოს იდენტიფიცირება ლოგიკის ცდომილების

`Allure Docs`
* https://github.com/allure-framework/allure-csharp-commons/wiki/Events
* https://github.com/allure-framework/allure-csharp
* https://www.automatetheplanet.com/test-automation-reporting-allure/
* https://docs.qameta.io/allure/