Codebase -დან Full Page Screen Capture -ით თიქეთების წამოღება და გითლაბზე დადებისთვის საჭიროა გაიარო შემდეგი ნაბიჯები

*  გქონდეს წვდომა Gitlab -ზე არსებულ ამ იშუებზე https://gitlab.com/doso/insurance/doso-core-insurance-group/doso-core-insurance/issues 
*  გქონდეს წვდომა Codebase -ზე არსებულ ამ თიქეთებზე https://doso-management.codebasehq.com/projects/dsinsurance/tickets?report=all
*  გქონდეს დაყენებული Chrome -ს Extension [Full Page Screen Capture](https://chrome.google.com/webstore/detail/full-page-screen-capture/fdpohaocaechififmbbbbbknoalclacl)

აღნიშნული ნაბიჯების გავლის შემდგომ:

მაგ: ![image](uploads/e8db913be5e1f95ca4155cdc825f9457/image.png)

ამ იშუს აქვს სათაური თიქეთის ნომერი, რომლითაც მოხდება მოძებნა Codebase -ზე

მაგ: ![image](uploads/c1ba9fe8029753376d0136dc256d3765/image.png)

Codebase -ზე თიქეთს გახსნი და გადაიღებ Full Page Screen Capture -ით
![image](uploads/c3e63b98924a2d62d310c42ea8a8e811/image.png)

გადაღებულ სქრინს Copy -ს გაუკეთებ და Gitlab -ზე არსებულ იშუში Paste -ებ

რის შემდეგაც Codebase -ზე არსებულ თიქეთს დახურავ.

