---

### `Chapter`: 20 (გარე სერვისებთან ინტეგრაცია)
### `Beneficiary`: TBCI
### `Feature`: 1 - მოტორის პოლისის რეგისტრაციისას Casco-ს პოლისის დასეივებისას ხდება TBCI-ს სერვისის გამოძახება, იმ შმეთხვევაში თუ:  
- პოლისი არ არის ვალიდური
- არ არის მონიშნული გამორიცხულია გაიდლაინიდან
- დამზღვევად მითითებულია ფიზიკური პირი
--------------
- 1. `Scenario:` Request-ში CalculateForTaxi (bool ტიპის) ველის მნიშვნელობის განსაზღვრა  
1.1.  
`Given:` **Casco-ს პროდუქტში** არჩეულია მნიშვნელობა, რომლის დასახელებაც შეიცავს **Taxi**-ს  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - **true**  
1.2.  
`Given:` **Casco-ს პროდუქტი** ცარიელია  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - **false**  
1.3.  
`Given:` **Casco-ს პროდუქტი** არჩეულია მნიშვნელობა, რომლის დასახელებაც **არ** შეიცავს **Taxi**-ს  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - **false**  
--------------
- 2. `Scenario:` Request-ში CalculateForTruck (Bool ტიპის) ველის მნიშვნელობის განსაზღვრა  
2.1.  
`Given:` დაზღვეული ობიექტის დეტალებში მითითებული **ავტოს ტიპის -> Type** არის **Truck**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - **true**  
2.2.  
`Given:` დაზღვეული ობიექტის დეტალებში მითითებული **ავტოს ტიპის -> Type** არის **Technique**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - **true**  
2.3.  
`Given:` დაზღვეული ობიექტის დეტალებში **ავტოს ტიპი** არ არის მითითებული  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - **false**  
2.4.  
`Given:` დაზღვეული ობიექტის დეტალებში **ავტოს ტიპი -> Type** არ არის არც **Truck** და არც **Technique**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - **false**  
2.5.  
`Given:` დაზღვეული ობიექტის დეტალებში **ავტოს ტიპი -> Type** არის N/A  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - **false**  
---------------
- 3. `Scenario:` Request-ში SpecTechniqueAndTruckType (int ტიპის) ველის მნიშვნელობის განსაზღვრა  
3.1.  
`Given:` დაზღვეული ობიექტის დეტალებში **ავტოს ტიპის -> CarType** ველის მნიშვნელობა იქნება **Sadle_Tracktor**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `1`   
3.2  
`Given:` დაზღვეული ობიექტის დეტალებში **ავტოს ტიპის -> CarType** ველის მნიშვნელობა იქნება **Trailer**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `2`  
3.3.  
`Given:` დაზღვეული ობიექტის დეტალებში **ავტოს ტიპის -> CarType** ველის მნიშვნელობა იქნება **Semi_Trailer**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `3`  
3.4.  
`Given:` დაზღვეული ობიექტის დეტალებში **ავტოს ტიპის -> CarType** ველის მნიშვნელობა არ არის განსაზღვრული  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `4`  
3.5.  
`Given:` დაზღვეული ობიექტის დეტალებში **ავტოს ტიპის -> CarType** ველის მნიშვნელობა არის ამ სამისგან გასხვავებული **Sadle_Tracktor**, **Trailer**, **Semi_Trailer**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `4`  
3.6  
`Given:` დაზღვეული ობიექტის დეტალებში **ავტოს ტიპის** არ არის არჩეული  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `4`
--------------
- 4. `Scenario:` Request-ში `SpecTechniqueAndTruckCoverageType` (int? ტიპის) ველის მნიშვნელობის განსაზღვრა   
4.1.  
`Given:` Casco-ს პროდუქტის დასახელება არის **დაფარვის პაკეტი A**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `1`  
4.2.  
`Given:` Casco-ს პროდუქტის დასახელება არის **დაფარვის პაკეტი B**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `2`  
4.3.  
`Given:` Casco-ს პროდუქტის დასახელება არის **დაფარვის პაკეტი C**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `3`   
4.4.  
`Given:` Casco-ს პროდუქტის დასახელება არის **დაფარვის პაკეტი D**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `4`  
4.5.  
`Given:` Casco-ს პროდუქტის დასახელება არის ამ ოტგასხვავებული **დაფარვის პაკეტი A**, **დაფარვის პაკეტი A** , **დაფარვის პაკეტი A** , **დაფარვის პაკეტი A**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `null`  
4.6.  
`Given:` Casco-ს პროდუქტი არ არის არჩეული  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `null`  
--------------
- 5. `Scenario:` Request-ში IncludeTrailer (bool ტიპის) ველის მნიშვნელობის განსაზღვრა  
5.1.  
`Given:` Motor Insurance Package-ის **IncludeTrailer** ველის მნიშვნელობა არის `კი`  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - **true**  
5.2.  
`Given:` Motor Insurance Package-ის **IncludeTrailer** ველის მნიშვნელობა არის `არა`   
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - **false**  
5.3.  
`Given:` Motor Insurance Package-ის **IncludeTrailer** ველის მნიშვნელობა არის `null`  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - **false** 
--------------
- 6. `Scenario:` Request-ში `CalculateForCreditCasco` (bool ტიპის) ველის მნიშვნელობის განსაზღვრა  
6.1.  
`Given:` PolicyBase-ის **ObjectType**  == 1347
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - **true**  
6.2.  
`Given:` PolicyBase-ის **ObjectType**  != 1347   
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - **false**  
------------------
- 7. `Scenario:` Request-ში `CreditCascoPackageType` (int? ტიპის) ველის მნიშვნელობის განსაზღვრა  
7.1.  
`Given:` Casco-ს პროდუქტის დასახელება არის **Silver Casco**  
`When:` სერვისის გამოძახებისას   
`Then:` მნიშვნელობა იქნება - `1`  
7.2.  
`Given:` Casco-ს პროდუქტის დასახელება არის **Gold Casco 21 წლიდან მძღოლებით**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `2`  
7.3.  
`Given:` Casco-ს პროდუქტის დასახელება არის **Gold Casco 18 წლიდან მძღოლებით**   
`When:` სერვისის გამოძახებისას   
`Then:` მნიშვნელობა იქნება - `2`  
7.4.  
`Given:` Casco-ს პროდუქტის დასახელება არ არის ამ სამიდან არცერთი **Silver Casco**, **Gold Casco 21 წლიდან მძღოლებით**, **Gold Casco 18 წლიდან მძღოლებით**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `null`  
----------------
- 8. `Scenario:` Request-ში `IsFromServiceCenter` (bool ტიპის) ველის მნიშვნელობის განსაზღვრა  
8.1.  
`Given:` MotorPackageBase-ის `IsFromServiceCenter` მნიშვნელობა არის - **true**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - **true**  
8.2.  
`Given:` MotorPackageBase-ის `IsFromServiceCenter` მნიშვნელობა არის - **false**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - **false**  
8.3.  
`Given:` MotorPackageBase-ის `IsFromServiceCenter` მნიშვნელობა არის - **null**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - **false**  
----------------
- 9. `Scenario:` Request-ში `FranchiseId` (int ტიპის) ველის მნიშვნელობის განსაზღვრა  
9.1.  
`Given:` Request-ში **CalculateForTaxi** ველის მნიშვნელობა თუ არის **true**  
`When:` სერვისის გამოძახებისას  
`Then:` `FranchiseId`-ის მნიშვნელობა იქნება - `111`  
9.2.  
`Given:` Request-ში **CalculateForTruck** ველის მნიშვნელობა თუ არის **true**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `111`   
9.3.  
`Given:` Request-ში **CalculateForCreditCasco** ველის მნიშვნელობა თუ არის **true**  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `111`  
9.4.    
`Given:` Request-ში **CalculateForCreditCasco** ველის მნიშვნელობა თუ არის **false**  
and **CalculateForTruck** ველის მნიშვნელობა არის **false**  
and **CalculateForTaxi** ველის მნიშვნელობა არის **false**  
and **CascoInsurancePackage.Deductible.ID** = 105  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `5`  
9.5.    
`Given:` Request-ში **CalculateForCreditCasco** ველის მნიშვნელობა თუ არის **false**  
and **CalculateForTruck** ველის მნიშვნელობა არის **false**  
and **CalculateForTaxi** ველის მნიშვნელობა არის **false**  
and **CascoInsurancePackage.Deductible.ID** = 106  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `6`  
9.6.    
`Given:` Request-ში **CalculateForCreditCasco** ველის მნიშვნელობა თუ არის **false**  
and **CalculateForTruck** ველის მნიშვნელობა არის **false**  
and **CalculateForTaxi** ველის მნიშვნელობა არის **false**  
and **CascoInsurancePackage.Deductible.ID** = 107  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `7`   
9.7.    
`Given:` Request-ში **CalculateForCreditCasco** ველის მნიშვნელობა თუ არის **false**  
and **CalculateForTruck** ველის მნიშვნელობა არის **false**  
and **CalculateForTaxi** ველის მნიშვნელობა არის **false**  
and **CascoInsurancePackage.Deductible.ID** not in (105, 106, 107)
and **CascoInsurancePackage.Deductible.DeductibleName.ContainsAnyOf("ნულოვანი", "უფრანშიზო")  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `0`   
9.8.    
`Given:` Request-ში **CalculateForCreditCasco** ველის მნიშვნელობა თუ არის **false**  
and **CalculateForTruck** ველის მნიშვნელობა არის **false**  
and **CalculateForTaxi** ველის მნიშვნელობა არის **false**  
and **CascoInsurancePackage.Deductible.ID** not in (105, 106, 107)
and **CascoInsurancePackage.Deductible.DeductibleName არ არის ("ნულოვანი", "უფრანშიზო")  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - `1`
----------------
- 10. `Scenario:` Response-ით დაბრუნებული ველების rate და loadingRate და ჩვენს მიერ Request-ში გადაცემული DiscountPercent ველის მნიშვნელობების მიხედვით SaleRate განსაზღვრა.  
SaleRate განსაზღვრა Response-ის პარამეტრების მიხედვით  
`Sale Rate = Rate / (1 - Dsocount) / Loading`  
10.1.  
`Given:` response.rate = 10.8  
`And` response.loadingRate = 1.2  
`And` request.DiscountPercent = 0.1  
`When:` Save  
`Then:` SaleRate-ის მნიშვნელობა უნდა იყოს - `10.0 %`   
10.2.  
`Given:` response.rate = 10.8  
`And` response.loadingRate = 0.0  
`And` request.DiscountPercent = 0.1  
`When:` Save  
`Then:` SaleRate-ის მნიშვნელობა უნდა იყოს - `12.0 %`  
10.3.  
`Given:` response.rate = 10.8  
`And` response.loadingRate = 0.0
`And` request.DiscountPercent = 0.0  
`When:` Save  
`Then:` SaleRate-ის მნიშვნელობა უნდა იყოს - `10.8 %`   
10.4.    
`Given:` response.rate = 0.0    
`And` response.loadingRate = 0.0  
`And` request.DiscountPercent = 0.0    
`When:` Save  
`Then:` SaleRate-ის მნიშვნელობა უნდა იყოს - `0.0 %`  
10.5.    
`Given:` response.rate = 9.8  
`And` response.loadingRate = 1.5  
`And` request.DiscountPercent = 0.2    
`When:` Save   
`Then:` SaleRate-ის მნიშვნელობა უნდა იყოს - `8.16 %`


---

### `Feature:` 2 - სოფლის მეურნეობის განვითარების სამინისტროში (APMA-ში) აგრო ზარელების შესახებ ინფორმაციის გაგზავნა

- `Scenario:` 1 - Request-ში Reduced_insurance_limit ველის მნიშვნელობის განსაზღვრა

`ექსპერტიზის შედეგად დადგენილი მოსავლიანობა (კგ)` ამ ველის მნიშვნელობა თუ არის 0-ზე მეტი

მაშინ   
Reduced_insurance_limit = `ექსპერტიზის შედეგად დადგენილი მოსავლიანობა (კგ)` * `ერთი კგ-ს საბაზრო ფასი`

`ექსპერტიზის შედეგად დადგენილი მოსავლიანობა (კგ)` ამ ველის მნიშვნელობა თუ არის 0 ან ცარიელი 

მაშინ   
Reduced_insurance_limit = `ერეალური მოსავლიანობა (კგ)` * `ერთი კგ-ს საბაზრო ფასი`


2.1.1   
`Given:` AgroReportedCalim-ის AgroInsuranceAccident-ის InsuranceLimit-ის მნიშვნელობა არის 0-ზე მეტი  
`And:` AgroReportedCalim-ში RealHarvestAmount არის  0-ზე მეტი   
`And:` AgroReportedCalim-ში KilogramMarketPrice არის  0-ზე მეტი
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - AgroReportedCalim.AgroInsuranceAccident. InsuranceLimit * KilogramMarketPrice  
2.1.2  
`Given:` AgroReportedCalim-ის AgroInsuranceAccident-ის InsuranceLimit-ის მნიშვნელობა არის `0.00`    
`And:` AgroReportedCalim-ში RealHarvestAmount არის  `4000.00`  
`And:` AgroReportedCalim-ში KilogramMarketPrice არის  `1.2`  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - 4000.00 * 1.2 = `4800.00`  
2.1.3.  
`Given:` AgroReportedCalim-ის AgroInsuranceAccident-ის InsuranceLimit-ის მნიშვნელობა არის `null`    
`And:` AgroReportedCalim-ში RealHarvestAmount არის  `4000.00`  
`And:` AgroReportedCalim-ში KilogramMarketPrice არის  `1.2`  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - 4000.00 * 1.2 = `4800.00`   
2.1.4.  
`Given:` AgroReportedCalim-ის AgroInsuranceAccident-ის InsuranceLimit-ის მნიშვნელობა არის `null`    
`And:` AgroReportedCalim-ში RealHarvestAmount არის  `0.00`  
`And:` AgroReportedCalim-ში KilogramMarketPrice არის  `1.2`  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - 0.00 * 1.2 = `0.00`  
2.1.5.   
`Given:` AgroReportedCalim-ის AgroInsuranceAccident-ის InsuranceLimit-ის მნიშვნელობა არის `null`    
`And:` AgroReportedCalim-ში RealHarvestAmount არის  `null`  
`And:` AgroReportedCalim-ში KilogramMarketPrice არის  `1.2`  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - 0.00 * 1.2 = `0.00`  
2.1.6.  
`Given:` AgroReportedCalim-ის AgroInsuranceAccident-ის InsuranceLimit-ის მნიშვნელობა არის `0.00`    
`And:` AgroReportedCalim-ში RealHarvestAmount არის  `4000.00`  
`And:` AgroReportedCalim-ში KilogramMarketPrice არის  `0.00`  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - 4000.00 * 0.00 = `0.00`  
2.1.7.  
`Given:` AgroReportedCalim-ის AgroInsuranceAccident-ის InsuranceLimit-ის მნიშვნელობა არის `0.00`    
`And:` AgroReportedCalim-ში RealHarvestAmount არის  `4000.00`  
`And:` AgroReportedCalim-ში KilogramMarketPrice არის  `null`  
`When:` სერვისის გამოძახებისას  
`Then:` მნიშვნელობა იქნება - 4000.00 * 0.00 = `0.00`

---

### `Chapter`: 30 (პროგრამული ვალიდაციები)
### `Beneficiary`: TBCI
### `Feature`: 1 - ვინ კოდის დუბლირების შეზღუდვა:  

- 1. `Scenario:` როცა ხდება ახალი Casco-ს პოლისის გაცემა, ამ დროს უნდა მოხდეს გადამოწმება, ხომ არ არსებობს უკვე პოლისი, რომლის მოქმედების პერიოდები, კვეთავს ახალი პოლისის პერიოდებს, და თუ კი უნდა შეიზღუდოს ახალი პოლისის გაცემა.  
1.1.  
`Given:` ძველი პოლისი არ არსებობს  
`When:` ხდება Motor-ის პაკეტის  და/ან Casco-ს პოლისის და/ან დაზღვეული ობიექტის Save  
`Then:` **არ უნდა** გამოვიდეს Exception - "მითითებული VIN კოდით უკვე არსებობს ჩანაწერი....."  
1.2.   
`Given:` ახალი პოლისის ამოქმედების თარიღი არის ძველი პოსლისის ამოქმედებასა და დასასრულს შორის (ორივე თარიღის ჩათვლით)  
`When:` ხდება Motor-ის პაკეტის  და/ან Casco-ს პოლისის და/ან დაზღვეული ობიექტის Save  
`Then:` **უნდა** გამოვიდეს Exception - "მითითებული VIN კოდით უკვე არსებობს ჩანაწერი....."  
1.3.  
`Given:` ახალი პოლისის პერიოდის დასასრული არის ძველი პოსლისის ამოქმედებასა და დასასრულს შორის (ორივე თარიღის ჩათვლით)  
`When:` ხდება Motor-ის პაკეტის  და/ან Casco-ს პოლისის და/ან დაზღვეული ობიექტის Save  
`Then:` **უნდა** გამოვიდეს Exception - "მითითებული VIN კოდით უკვე არსებობს ჩანაწერი....."  
1.4.  
`Given:` ახალი პოლისის ამოქმედების თარიღი მეტია ძველი პოსლისის დასასრულის  
`When:` ხდება Motor-ის პაკეტის  და/ან Casco-ს პოლისის და/ან დაზღვეული ობიექტის Save  
`Then:` **არ უნდა** გამოვიდეს Exception - "მითითებული VIN კოდით უკვე არსებობს ჩანაწერი....."    
1.5.  
`Given:` ახალი პოლისის დასრულების თარიღი ნალებია ძველი პოსლისის ამოქმედების თარიღის  
`When:` ხდება Motor-ის პაკეტის  და/ან Casco-ს პოლისის და/ან დაზღვეული ობიექტის Save  
`Then:` **არ უნდა** გამოვიდეს Exception - "მითითებული VIN კოდით უკვე არსებობს ჩანაწერი....."





### `Chapter`: 50 (ავტომატური პროცესები)
`Beneficiary`: TBCI
### `Feature`: 1 - ინვოისების ავტომატური გენერირება 

- **Scenario**: კონტრაგენტი არის `იურიდიული პირი` და 2 პოლისის `დამზღვევი` რომლიდანაც ერთს 5 დღეში უწევს გადახდა და მეორეს 7 დღეში  
**Execution**: Dynamic Script Schedule  
**About**: ინვოისების ავტომატური გენერირების ლოგიკაში, რომელიც **Dynamic Script Schedule** -დან გენერირდება, მოთხოვნილი არის არსებული ლოგიკის ცვლილება, ნაცვლად იმისა რომ ინვოისები დაგენერირდეს Corporate და SME მიმართულების მქონე პოლისებზე, უნდა დაგენერირდეს პოლისებზე რომლებზეც სექტორი იქნება Corporate და დამზღვევი იურიდიული პირი  
**Wiki** -ს ლინკი: [ინვოისების ავტომატური გენერირება და მეილზე გაგზავნა](https://gitlab.com/doso/insurance/tbc-insurance/applicationsupportgroup/private/application-support-execution/-/wikis/%E1%83%98%E1%83%9C%E1%83%95%E1%83%9D%E1%83%98%E1%83%A1%E1%83%94%E1%83%91%E1%83%98%E1%83%A1%20%E1%83%90%E1%83%95%E1%83%A2%E1%83%9D%E1%83%9B%E1%83%90%E1%83%A2%E1%83%A3%E1%83%A0%E1%83%98%20%E1%83%92%E1%83%94%E1%83%9C%E1%83%94%E1%83%A0%E1%83%98%E1%83%A0%E1%83%94%E1%83%91%E1%83%90%20%E1%83%93%E1%83%90%20%E1%83%9B%E1%83%94%E1%83%98%E1%83%9A%E1%83%96%E1%83%94%20%E1%83%93%E1%83%90%E1%83%92%E1%83%96%E1%83%90%E1%83%95%E1%83%9C%E1%83%90)  
1.1.  
`Given`: ერთ პოლისზე დავალიანება გრაფიკზე არის 10 ლარი და მეორეზე 5 ლარი  
`When:` სკრიპტი დააგენერირებს ინვოისებს  
`Then:` `ინვოისის დაგენერირებული თანხა` იქნება 15 ლარი  
1.2.  
`Given`: პოლისის გრაფიკზე `ტრანშის თანხა` არის 15 ლარი  
`And:` `გაუქმებული თანხა` არის 3  
`And:` `გადახდილი თანხა`არის 3  
`And:`  დავალიანება 9 ლარი   
`When:` სკრიპტი დააგენერირებს ინვოისს  
`Then:` ინვოისის დაგენერირებული თანხა იქნება 9 ლარი  
1.3.  
`Given:` პოლისის გრაფიკზე ტრანშის თანხა არის 15 ლარი და გაუქმებული თანხა ან გადახდილი თანხა არის 3  
`When:` სკრიპტი დააგენერირებს ინვოისებს  
`Then:` ინვოისის დაგენერირებული თანხა იქნება 13 ლარი  
1.4.  
`Given`: ერთი `პოლისის პრემიის ვალუტა` არის ლარი, მეორესი კი დოლარი  
`When:` სკრიპტი დააგენერირებს ინვოისებს  
`Then:` დაგენერირდება ორი სხვადასხვა ინვოისი ვალუტების მიხედვით, ლარში და დოლარში.  
1.5.  
`Given`: ორი პოლისი არის ორი სხვადასხვა დამზღვევით  
`When:` სკრიპტი დააგენერირებს ინვოისებს  
`Then:` დაგენერირდება თითო ინვოისი თითო დამზღვევისთვის, ჯამში 2  
1.6.  
`Given`: პოლისს მიმდინარე ტრანშზე დავალიანება აქვს 15 ლარი და წინა ტრანშზე 10 ლარი  
`When:` სკრიპტი დააგენერირებს ინვოისებს  
`Then:` ინვოისის დაგენერირებული თანხა იქნება 25 ლარი  
1.7.  
`Given`: პოლისს მიმდინარე ტრანშზე დავალიანება აქვს 15 ლარი წინა ტრანშზე 10 ლარი და ერთ-ერთ ტრანშზე გადახდილი თანხა არის 10 ლარი  
`When:` სკრიპტი დააგენერირებს ინვოისებს  
`Then:` ინვოისის დაგენერირებული თანხა იქნება 15 ლარი  
1.8.  
`Given`: დამზღვევს არ აქვს მონიშნული `მთელი თვის ინვოისის გენერირება`  
`When:` სკრიპტი დააგენერირებს ინვოისებს  
`Then:` ინვოისში მოხვდება მხოლოდ ის პოლისი რომელსაც `ტრანშის თარიღი` აქვს 5 დღეში  
1.9.  
`Given`: დამზღვევს მონიშნული აქვს მთელი თვის ინვოისის გენერირება    
`When:` სკრიპტი დააგენერირებს ინვოისებს  
`Then:` ინვოისში მოხვდება ორივე პოლისი  
1.10.  
`Given`: დამზღვევს არ აქვს მონიშნული მთელი თვის ინვოისის გენერირება  
`And:` ორივე პოლისის ტრანშის თარიღი არის 5 დღეში  
`When:` სკრიპტი დააგენერირებს ინვოისს  
`Then:` დაგენერირდება ორი ინვოისი, თითო ინვოისი თითო პოლისისთვის   
1.11.  
`Given`: დამზღვევს მონიშნული აქვს `არ გაეგზავნოს ავტომატური ინვოისი`  
`When:` სკრიპტი დააგენერირებს ინვოისებს  
`Then:` ამ კონტრაგენტზე ინვოისი არ დაგენერირდება  
1.12.  
`Given`: დამზღვევს არ მონიშნული აქვს არ გაეგზავნოს ავტომატური ინვოისი  
`When:` სკრიპტი დააგენერირებს ინვოისებს  
`Then:` ამ კონტრაგენტზე ინვოისი დაგენერირდება  
1.13.  
`Given`: ერთი `პოლისი არ არის ვალიდური`  
`Or:` ერთი `პოლისი გაუქმებულია`  
`Or:` ერთი `პოლიბი არის დაექსპაირებული`  
`When:` სკრიპტი დააგენერირებს ინვოისებს  
`Then:` ინვოისში ეს პოლისი არ მოხვდება  
1.14.   
`Given`: ორივე პოლისი არ არის ვალიდური  
`Or:` ორივე პოლისი გაუქმებულია  
`Or:` ორივე პოლისი არის დაექსპაირებული  
`When:` სკრიპტი დააგენერირებს ინვოისებს  
`Then:` ამ კონტრაგენტზე ინვოისი არ დაგენერირდება  
1.15.  
`Given`: პოლისების `მიმართულება` არის Corporate ან SME ---> `სექტორი` არის Corporate და დამზღვევი იურიდიული პირი  
`When:` სკრიპტი დააგენერირებს ინვოისებს  
`Then:` ამ პოლისებზე ინვოისები დაგენერირდება  
1.16.  
`Given`: `პოლისის გრაფიკი` რომელსაც ტრანშის თარიღი აქვს 5 დღეში არ არის აქტიური  
`When:` სკრიპტი დააგენერირებს ინვოისებს  
`Then:` ინვოისში ასეთი პოლისები არ მოხვდება  
1.17.  
`Given`: პოლისის გრაფიკზე რომელსაც ტრანშის თარიღი აქვს 5 დღეში `დავალიანება` არის 0  
`When:` სკრიპტი დააგენერირებს ინვოისებს  
`Then:` ინვოისში ასეთი პოლისები არ მოხვდება/არ დაგენერირდება  
1.18.  
`Given`: ორივე პოლისის ლიცენზია არის PersonalAccidentPackage  
`When:` სკრიპტი დააგენერირებს ინვოისს  
`Then:` ინვოისი დაგენერირდება საბეჭდი ფორმით **ინვოისი PA**  
1.19.  
`Given`: მხოლოდ ერთი პოლისის ლიცენზია არის PersonalAccidentPackage  
`When:` სკრიპტი დააგენერირებს ინვოისს  
`Then:` დაგენერირდება 2 ინვოისი, 1 PersonalAccidentPackage -ისთვის და 1 სხვა ლიცენზიისთვის

 

### `Feature`: 2 - დაზღვევის მენეჯერის შეფასება
**Execution**: Dynamic Script Schedule

- 1. **Scenario**: პოლისზე პირველი ტრანში არის გადახდილი  
1.1  
`Given:` პოლისის `სექტორი` არის Retail  
`When:` Dynamic Script Runner დაატრიგერებს სქედულს  
`Then:` სკრიპტი გამოიძახებს სერვისს  
1.2  
`Given:` პოლისის სექტორი არ არის Retail  
`When:` Dynamic Script Runner დაატრიგერებს სქედულს  
`Then:` სკრიპტი არ გამოიძახებს სერვისს  
1.3    
`Given:` პოლისის `მიმართულება` არის Digital  
`When:` Dynamic Script Runner დაატრიგერებს სქედულს  
`Then:` სკრიპტი არ გამოიძახებს სერვისს  
1.4  
`Given:` პოლისის მიმართულება არ არის Digital  
`When:` Dynamic Script Runner დაატრიგერებს სქედულს  
`Then:` სკრიპტი გამოიძახებს სერვისს   
1.5  
`Given:` კონკრეტულ პოლისზე უკვე მოხდა სერვისის გამოძახება  
`When:` Dynamic Script Runner დაატრიგერებს სქედულს  
`Then:` სკრიპტი არ გამოიძახებს სერვისს


ჩვენი მხრიდან გასაგზავნი Request -ის დეტალები: 

``` Json
SecretUserName = სტანდარტულად რასაც გადავცემთ ნებისმიერი სერვისის გამოძახებისას
SecretPassword = სტანდარტულად რასაც გადავცემთ ნებისმიერი სერვისის გამოძახებისას (სატესტოზე შესაბამისი, და Prod-ისთვის შესაბამისი)
Requester = "InsCore"
UserId = ""
PhoneNumber = დამზღვევის ტელეფონის ნომერი, რომელიც პაკეტში არის მითითებული, თუ ეს ველი ცარიელი იქნება წამოვიღოთ დამზღვევის Default Number 
ClientFullName = დაზღვეულის სახელი, გვარი
CarGovNumber = ავტომობილის სახელმწიფო ნომერი
ServiceTypeId = 7
ObjectId = პასუხისმგებელი პირის სახელი და გვარი
ProductId = დოკუმენტის ID, იმ ჩანაწერის ნომერი, ამ შემთხვევაში პოლისის ნომერი, რომელზეც მოხდა ქმედება და ამის შედეგად გაიგზავნა სმს

```

### `Chapter`: 3 (ზარალები)
`Beneficiary`: TBCI
### `Feature`: 1 - დისტანციური ანაზღაურება - **Payment By Phone**  

**Execution**: Dynamic Action - **Payment By Phone** 


- 1. **Scenario**: სადაზღვევო შემთხვევა არის ქონების ტიპის  
1.1  
`Given:` `პოლისის ტიპი არ არის ქონება`  
`And:` პოლისის ტიპი არ არის კასკო  
`And:` პოლისის ტიპი არ არის PPI  
`And:` პოლისის ტიპი არ არის თრეველი    
`And:` პოლისის  ტიპი არ არის MTPL  
`When:` მომხმერებელი გამოიყენებს ღილაკს  **Payment By Phone**  
`Then:` გამოვა ტექსტური შეტყობინება: **სატელეფონო დარეგულირება შესაძლებელია მხოლოდ შემდეგი პოლისის ტიპის ზარალებისთვის: Casco, MTPL, PPI, Property, Travel**  
1.2  
`Given:` სტატუსი არ არის `მზად არის სატელეფონო დარეგულირებისთვის`  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` გამოვა ტექსტური შეტყობინება: **ზარალის დარეგულირება უნდა იყოს სტატუსში - მზად არის დარეგულირებისთვის**   
1.3  
`Given:` `ასანაზღაურებელი თანხა` მეტია 2000 ლარზე  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` გამოვა ტექსტური შეტყობინება: **ასანაზღაურებელი თანხა არ უნდა იყოს 2000 ლარზე მეტი**   
1.4  
`Given:` `პოლისის მოსარგებლე არის იურიდიული პირი`  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` გამოვა ტექსტური შეტყობინება: **პოლისის მოსარგებლე უნდა იყოს ფიზიკური პირი**   
1.5  
`Given:` პოლისის მოსარგებლე არის [შავ სიაში](Glossary.md#კონტრაგენტი-არის-შავ-სიაში)  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` გამოვა ტექსტური შეტყობინება **პოლისის მოსარგებლე შავ სიაშია**  
1.6  
`Given:` `პოლისის მოსარგებლე` და `თანხის მიმღები` არ არის ერთიდაიგივე ფიზიკური პირი  
**Or**: `ანაზღაურება არ ხდება ინვოისის მიხედვით`  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` გამოვა ტექსტური შეტყობინება: **პოლისის მოსარგებლე და თანხის მიმღები უნდა იყოს ერთიდაიგივე ფიზიკური პირი, ან თანხა უნდა ირიცხებოდეს სერვის ცენტრში**  
1.7  
`Given:` პოლისის ტიპი არის კასკო  
`And:` სტატუსი არის მზად არის სატელეფონო დარეგულირებისთვის  
`And:` ასანაზღაურებელი თანხა <= 2000 ლარზე  
`And:` პოლისის `მოსარგებლე არის ფიზიკური პირი`  
`And:` პოლისის მოსარგებლე არ არის [შავ სიაში](Glossary.md#კონტრაგენტი-არის-შავ-სიაში)  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` მოხდება გადახდის სერვისის გამოძახება  
1.8  
`Given:` პოლისის ტიპი არის ქონება  
`And:` სტატუსი არის მზად არის სატელეფონო დარეგულირებისთვის  
`And:` ასანაზღაურებელი თანხა <= 2000 ლარზე  
`And:` პოლისის `მოსარგებლე არის ფიზიკური პირი`  
`And:` პოლისის მოსარგებლე არ არის [შავ სიაში](Glossary.md#კონტრაგენტი-არის-შავ-სიაში)  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` მოხდება გადახდის სერვისის გამოძახება  
1.9  
`Given:` `პოლისის მოსარგებლე` და `თანხის მიმღები` არის ერთიდაიგივე ფიზიკური პირი  
**Or**: `ანაზღაურება არ ხდება ინვოისის მიხედვით`  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` მოხდება გადახდის სერვისის გამოძახება 
-----
- 2. **Scenario**: სადაზღვევო შემთხვევა არის PPI -ის ტიპის  
2.1  
`Given:` ჩანაწერი არ არის დამტკიცებულია სტატუსში
`And:` ჩანაწერი არ არის მზად არის სატელეფონო დარეგულირებისთვის სტატუსში  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` გამოვა ტექსტური შეტყობინება: **ზარალის დარეგულირება უნდა იყოს სტატუსში - დამტკიცებულია**   
------
- 3. **Scenario**: სადაზღვევო შემთხვევა არის თრეველის ტიპის   
3.1   
`Given:` სტატუსი არ არის მზად არის სატელეფონო დარეგულირებისთვის  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` გამოვა ტექსტური შეტყობინება: **ზარალის დარეგულირება უნდა იყოს სტატუსში - მზად არის დარეგულირებისთვის**  
3.2  
`Given:` ასანაზღაურებელი თანხა მეტია 2000 ლარზე  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` გამოვა ტექსტური შეტყობინება: **ასანაზღაურებელი თანხა არ უნდა იყოს 2000 ლარზე მეტი**  
3.3  
`Given:` პოლისის მოსარგებლე არის [შავ სიაში](Glossary.md#კონტრაგენტი-არის-შავ-სიაში)  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` გამოვა ტექსტური შეტყობინება **პოლისის მოსარგებლე შავ სიაშია** 
-----
- 4. **Scenario**: ღილაკი **Payment By Phone** გამოყენებისთვის ყველა პირობა სრულდება      
4.1    
`Given:` სადაზღვევო შემთხვევა არის ქონების ტიპის  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**   
`Then:` შექმნილი PRF -ის `გამგზავნის ანგარიშის ნომერი` იქნება GE55TB7778736020100007  
4.2  
`Given:` სადაზღვევო შემთხვევა არ არის ქონების ტიპის  
`And:` სადაზღვევო შემთხვევა არ არის თრეველის ტიპის   
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**   
`Then:` შექმნილი PRF - ის გამგზავნის ანგარიშის ნომერი იქნება GE66TB7778736020100003  
4.3  
`Given:` სადაზღვევო შემთხვევა არის თრეველის ტიპის  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` შექმნილი PRF -ის `გამგზავნის ანგარიშის ნომერი` იქნება GE71TB7778736020100010
------
- 5 **Scenario**: სადაზღვევო შემთხვევა არის მოტორის ტიპის  
5.1  
`Given:` ასანაზღაურებელი თანხა მეტია 10 000 ლარზე  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` გამოვა ტექსტური შეტყობინება: **ასანაზღაურებელი თანხა არ უნდა იყოს 10000 ლარზე მეტი**  
5.2  
`Given:`  დამზღვევი არ არის ფიზიკური პირი  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` გამოვა ტექსტური შეტყობინება **პოლისის მფლობელი უნდა იყოს ფიზიკური პირი**  
5.3  
`Given:` პოლისის დამზღვევი არის [შავ სიაში](Glossary.md#კონტრაგენტი-არის-შავ-სიაში)  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` გამოვა ტექსტური შეტყობინება **პოლისის მფლობელი შავ სიაშია**  
5.4  

5.5  
`Given:` ზარალი არ არის *დამტკიცებულია სტატუსში*  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` გამოვა ტექსტური შეტყობინება **ზარალის დარეგულირება უნდა იყოს დამტკიცებულ სტატუსში**  
5.6  
`Given:` პოლისის მოსარგებლე არის ავტომობილის მესაკუთრე ( BasePerson.IsUnknownBenefitiary = True)  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` გამოვა ტექსტური შეტყობინება **მოსარგებლე არ უნდა იყოს ავტომობილის მესაკუთრე**  
5.7  
`Given:` ასანაზღაურებელი თანხა ნაკლებია ან ტოლია 10 000 ლარზე  
`And:` დამზღვევი არის ფიზიკური პირი  
`And:` დამზღვევი არ არის [შავ სიაში](Glossary.md#კონტრაგენტი-არის-შავ-სიაში)  
`And:` პოლისის ტიპი არის კასკო  
`And:` ზარალის არის დამტკიცებულია სტატუსში  
`And:` პოლისის მოსარგებლე არ არის *ავტომობილის მესაკუთრე*  
`When:` მომხმარებელი გამოიყენებს ღილაკს **Payment By Phone**  
`Then:` მოხდება სერვისის გამოძახება

### `Feature`: 2 - დისტანციური ანაზღაურება  - **Claim Web Settlement**

**Execution**: Dynamic Action

- 1. **Scenario**: სადაზღვევო შემთხვევა არის მოტორის ტიპის  
1.1  
`Given:` ასანაზღაურებელი თანხა მეტია 10 000 ლარზე  
`When:` მომხმარებელი გამოიყენებს ღილაკს  
`Then:` გამოვა ტექსტური შეტყობინება: **ასანაზღაურებელი თანხა არ უნდა იყოს 10000 ლარზე მეტი**  
1.2  
`Given:`  დამზღვევი არ არის ფიზიკური პირი  
`When:` მომხმარებელი გამოიყენებს ღილაკს  
`Then:` გამოვა ტექსტური შეტყობინება **პოლისის მფლობელი უნდა იყოს ფიზიკური პირი**  
1.3  
`Given:` პოლისის დამზღვევი არის [შავ სიაში](Glossary.md#კონტრაგენტი-არის-შავ-სიაში)  
`When:` მომხმარებელი გამოიყენებს ღილაკს  
`Then:` გამოვა ტექსტური შეტყობინება **პოლისის მფლობელი შავ სიაშია**  
1.4  
`Given:` პოლისის ტიპი არ არის კასკო  
`When:` მომხმარებელი გამოიყენებს ღილაკს  
`Then:` გამოვა ტექსტური შეტყობინება **პოლისის ტიპი უნდა იყოს კასკო**  
1.5  
`Given:` ზარალი არ არის *დამტკიცებულია სტატუსში*  
`When:` მომხმარებელი გამოიყენებს ღილაკს  
`Then:` გამოვა ტექსტური შეტყობინება **ზარალის დარეგულირება უნდა იყოს დამტკიცებულ სტატუსში**  
1.6  
`Given:` პოლისის მოსარგებლე არის [ავტომობილის მესაკუთრე](Glossary.md#ფიზიკური-პირი-არის-ავტომობილის-მესაკუთრე)  
`When:` მომხმარებელი გამოიყენებს ღილაკს  
`Then:` გამოვა ტექსტური შეტყობინება **მოსარგებლე არ უნდა იყოს ავტომობილის მესაკუთრე**  
1.7  
`Given:` ასანაზღაურებელი თანხა ნაკლებია ან ტოლია 10 000 ლარზე  
`And:` დამზღვევი არის ფიზიკური პირი  
`And:` დამზღვევი არ არის [შავ სიაში](Glossary.md#კონტრაგენტი-არის-შავ-სიაში)  
`And:` პოლისის ტიპი არის კასკო  
`And:` ზარალის არის დამტკიცებულია სტატუსში  
`And:` პოლისის მოსარგებლე არ არის *ავტომობილის მესაკუთრე*  
`When:` მომხმარებელი გამოიყენებს ღილაკს  
`Then:` მოხდება სერვისის გამოძახება 


**About**: https://gitlab.com/doso/insurance/tbc-insurance/applicationsupportgroup/application-support/-/issues/3880#note_357735431

**1**  
**Feature**: ფაკულტატორული გადაზღვევის რეგისტრირებისას ველების მნიშვნელობების ავტომატურად შევსება

**ტერმინთა განმარტება**: 

* `Reinsurers Share` -  **LastReinsLayer.ReinsurersCession**  
* `არ არის რედაქტირებადი` - **AllowEdit = False**  
* `Gross Reinsurance Premium` - **LastReinsLayer.ReinsurancePremium**  
* `Reinsurance Commission` - **LastReinsLayer.ReinsuranceComission**
* `Reinsurers Share Amount` - **LastReinsLayer.ReinsurersShareInSuminsured**  
* `პოლისის სადაზღვევო თანხა` - **PolicyBase.SumInsured**  
* `პოლისის ვალუტი კურსი` - **GetRate(policy.EffectiveDate, policy.GrossWrittenPremiumCurrency)**  
* `გადაზღვევის ვალუტის კურსი` - **GetRate(reins.EffectiveDate, reins.ReinsurancePremiumCurrency)**  
* `Reinsurance Commission %` - **LastReinsLayer.ReinsurersCommission**  

**Scenario**: ხდება ახალი გადაზღვევის რეგისტრირება  
**Or**: ხდება არსებული გადაზღვევის რედაქტირება  

1.1  
`Given:` AllowEditReinsurersShareAmount = False  
`Or:` AllowEditReinsurersShareAmount = Null  
`Then:` **Reinsurers Share Amount** **არ არის რედაქტირებადი** 

1.2  
`Given:` AllowEditReinsurersShareAmount = False  
`Or:` AllowEditReinsurersShareAmount = Null   
`When:` **Reinsurers Share** --> OnChange   
`Then:` `Reinsurers Share Amount = (პოლისის სადაზღვევო თანხა * პოლისის ვალუტის კურსზე) / (Reinsurers Share * გადაზღვევის ვალუტის კურსზე)`

1.3  
`Given:` AllowEditReinsurersShareAmount = True   
`Then:` Reinsurers Share Amount რედაქტირებადია  
`And:` Reinsurers Share არ არის რედაქტირებადი

1.4  
`Given:` AllowEditReinsurersShareAmount = True  
`When:` Reinsurers Share Amount --> OnChange  
`Then:` `Reinsurers Share = (Reinsurers Share Amount * გადაზღვევის ვალუტის კურსზე) / (პოლისის სადაზღვევო თამხა * პოლისის ვალუტის კურსზე)`

1.5  
`Given:` AllowEditGrossReinsurancePremium = False  
`Or:` AllowEditGrossReinsurancePremium = Null  
`Then:` **Gross Reinsurance Premium** არ არის რედაქტირებადი  

1.6  
`Given:` AllowEditGrossReinsurancePremium = False  
`Or:` AllowEditGrossReinsurancePremium = Null  
`When:` Reinsurance Rate --> OnChange  
`Then:` `Gross Reinsurance Premium = Reinsurers Share Amount * Reinsurance Rate`

1.7  
`Given:` AllowEditGrossReinsurancePremium = True  
`Then:` Reinsurance Rate არ არის რედაქტირებადი  
`And:` Gross Reinsurance Premium რედაქტირებადია 

1.8  
`Given:` AllowEditGrossReinsurancePremium = True  
`When:` Gross Reinsurance Premium --> OnChange  
`Then:` `Reinsurance Rate = Gross Reinsurance Premium / Reinsurers Share Amount`

1.9  
`Given:` AllowEditReinsuranceCommission = False  
`Or:` AllowEditReinsuranceCommission = Null  
`Then:` **Reinsurance Commission** არ არის რედაქტირებადი  

1.10  
`Given:` AllowEditReinsuranceCommission = False  
`Or:` AllowEditReinsuranceCommission = Null  
`When:` **Reinsurance Commission %** --> OnChange  
`Then:` `Reinsurance Commission = Gross Reinsurance Premium * Reinsurance Commission %` 

1.11  
`Given:` AllowEditReinsuranceCommission = True  
`Then:` Reinsurance Commission % არ არის რედაქტირებადი  
`And:` Reinsurance Commission რედაქტირებადია  

1.12  
`Given:` AllowEditReinsuranceCommission = True  
`When:` Reinsurance Commission --> OnChange  
`Then:` `Reinsurance Commission % = Reinsurance Commission / Gross Reinsurance Premium

1.13  
`Given:` Schedule = True  
`And:` Gross Reinsurance Premium <= 0  
`When:` OnSaving  
`Then:` ვერ მოხდება ჩანაწერის შენახვა  

1.14  
`Given:` Schedule = True  
`And:` Gross Reinsurance Premium > 0  
`When:` OnSaving  
`Then:` ჩანაწერი წარმატებით შეინახება  

1.15  
`Given:` Schedule = False  
`Or:` Schedule = Null  
`When:` Onsaving  
`Then:` ჩანაწერი წარმატებით შეინახება

> This is a note which needs your attention, but it's not super important.
> [!NOTE]








> [!WARNING]
> This is a warning containing some important message.

> [!CAUTION]
> This is a warning containing some **very** important message.
