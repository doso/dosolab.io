# Glossary

## აქტიური პოლისი

პოლისი რომელიც:

- არ არის წაშლილი
- არის ვალიდური  
- პერიოდის დასაწყისი ნაკლებია ან ტოლი მიმდინარე თარიღზე
- პოლისის გაუქმების თარიღი ან/და პერიოდის დასასრული მეტია ან ტოლი მიმდინარე თარიღზე
- არ არის ანულირებული, ანუ გაუქმების თარიღი არ უდრის პერიოდის დასაწტისს

```csharp
//CSHARP
var ActivePolicies = uow.Query<PolicyBase>()
                        .Where(x => x.FromDate <= DateTime.Now && x.ToDate > DateTime.Now && x.ExpiredOn == null && x.IsValid);
```

```sql  
--SQL
select id, policynumber from policybase
where expiredon is NULL
and policyisvalid
and fromdate <= now()::DATE
and coalesce(cancellationdate, todate) >= now()
and coalesce(cancellationdate, todate) != fromdate
```

## აქტიური აგენტი

უნდა არსებობდეს რესურსი (Human Resource) რომელიც:

- არ არის წაშლილი
- პოზიციაში მონიშნულია რომ არის აგენტი
- ააქტიურობის დასაწყისი ნაკლებია ან ტოლი მიმდინარე თარიღის
- აქტიურობის დასასრული მეტია მიმდინარე თარითზე
- თანამშრომლის გაუქმების თარიღი არ არის მითითებული ან მეტია მიმინარე თარიღზე
- თანამშრომლის ჩანაწერი არ არის წაშლილი.

```sql
--SQL
select h.id  from humanresource h
join position p on p.id = h.position
join employee e on e.id = h.employee
where h.expiredon is null
and p.isagent
and h.activefrom <= now()::DATE
and h.activeuntil > now()
and coalesce(e.expiredate, now()::Date) > now()::Date
and e.expiredon is null
```

## პოლისის გამომუშავება ლარში

გამომუშავება არის პოლისის პრემიის ის ნაწილი, რომელიც ეკუთვნის პოლისის ამოქმედების დღიდან - მიმდინარე დღემდე პერიოდს.

```sql
select  CASE WHEN (SELECT ExpireComment
                      FROM CustomClass4CancelPolicy
                      WHERE ID = p.CustomClass4CancelPolicy
                            AND ExpireComment = 15
                      LIMIT 1) IS NOT NULL  AND p.CancellationDate <= now()
             THEN (p.GrossWrittenPremium * GetRate(p.EffectiveDate, p.GrossWrittenPremiumCurrency))
           ELSE CASE WHEN p.EffectiveDate < now()
             THEN (DATEDIFF('day', p.EffectiveDate, least(COALESCE(p.CancellationDate
                     + INTERVAL '1 day', p.ToDate + INTERVAL '1 day'),
                                  DATEADD('day', 1, now()))) * 1.0)
                  / DATEDIFF('day', p.EffectiveDate - interval '1 day', p.ToDate)
                  * p.GrossWrittenPremium * GetRate(p.EffectiveDate, p.GrossWrittenPremiumCurrency)
                ELSE 0  END   END  from policybase p
                where id = 'policybase.ID'
```

## მიმდინარე დღეს გადახდა დაფიქსირებული პოლისები - Epayment ჩარიცხვის მიხედვით

სელექტი აბრუნეებს Epaymant ტრანზაქციით გადახსილ პოლისის ID-ებს რომლებზეც გადახდა დაფიქსირდა მიმდინარე დღეს.
ბრუნდება როგორც პოლისზე, ისე პირგასამტეხლოზე (ან სხვა გადასახდელზე) დაფიქსირებული თანხები

```sql
select  p.id from distributedamountbase d
left join distributedamount2schedule ds on ds.id = d.id
left join schedule s  on s.id = ds.schedule
left join policybase p on p.id = s.insurancepolicy
left join bankstatement2redistribute br on br.id = ds.bankstatement2redistributeex
join financialtransaction f on f.id = br.bankstatement
where d.transactiontype = 2 and d.createdon::date = now()::Date
GROUP BY  f.id, p.id, f.amount

UNION ALL
select  c.policy from receivablebase rb
join commitmentbase c on c.id = rb.id
join commitmentschedule cs on cs.commitmentbase = rb.id
join distributedamount2schedule dcs on dcs.schedule = cs.id
join distributedamount2commitmentschedule sdf on sdf.id = dcs.id
join bankstatement2redistribute br on br.id = sdf.redistribution
join financialtransaction f on f.id = br.bankstatement
where f.transactiontype = 2 and f.createdon::date = now()::Date;
```

## შეუმოწმებელი სტატუსის მქონე ხელშეკრულება

Agreement Controlling Object-ის Status - 0

## ხარვეზიანი ხელშეკრულება

Agreement Controlling Object-ის Status - 4

```csharp
//CSHARP
Agreement
.Where(x => x.ხარვეზიანი == true)
```

```sql
--SQL
select * from Agreement where ხარვეზიანი = 1
```

## დასტურის მოლოდინში

ხელშეკრულება - Agreement Controling Object-ის Status - 5

## დადასტურებული ხელშეკრულება  

Agreement Controling Object-ის Status - 6

## გადასამოწმებელი ხელშეკრულება  

Agreement Controling Object-ის Status - 7

## შეცდომის ტიპი  

PolicyControllingMistakeFine (31,32,33,34,35,36,37,38)

## Agreement Controlling Object  

Pop Up Window სადაც ხდება კონტროლინგის ყველა ოპერაცია

## Create Policy  

ღილაკი, რომლის საშუალებით ხდება ხელშეკრულებიდან ახალი პოლისის შექმნა

## ბევრ პოლისზე კონტროლინგის დასტური  

ღილაკი, რომლის საშუალებით შესაძლებელია ბევრ პოლისზე მოინიშნოს ერთიდაიგივე ტიპის ხარვეზი

## ოპერატორის დასტური  

როდესაც სადაზღვევოს ოპერატორი გაასწორებს მითითებულ ხარვეზს, *Policy controlling detail*-ის Operator Approve = True

## კონტროლინგის დასტური  

როდესაც კონტროლინგის ოპერატორი გადაამოწმების შედეგად ხელშეკრულებაზე უკვე გაგზავნილი ხარვეზი შესწორებულია, *Policy controlling detail*-ის Controling Approve = True

## დასრულებული  

ხელშეკრულება რომელსაც კონტროლინგის სტატუსი აქვს "დადასტურებული"

## ერთი ტიპის  

იგივე "შეცდომის ტიპი"-ები რასაც ვუთითებთ ხელშეკრულების შემთხვევაში, შესაძლებელია ხელშეკრულება იყოს ხარვეზიანი და პოლისები არა, ან პირიქით

--ToDo  

Feature: 50.TBCI.1

## იურიდიული პირი  

ContrahentBase.ClientType = 1

## დამზღვევი

PolicyBase.Contrahent

## არ გაეგზავნოს ავტომატური ინვოისი

ContrahentBase.DoNotSendAutomaticInvoices

## მთელი თვის ინვოისის გენერირება

Contrahentbase.GenerateSingleInvoicesForTheEntireMonth

## პოლისი არ არის ვალიდური

Policybase.PolicyIsValid = False

## პოლისი გაუქმებულია

Policybase.Status In (2, 4) ან Policybase.Cancellationdate < Now  

## პოლისი არ არის დაექსპაირებული  

Policybase.Expiredon Is Not Null  

## პოლისის მიმართულება  

Policybase.Direction  

## პოლისის სექტორი  

Policybase.Sector`  

## აქტიური გრაფიკი  

Schedule.IsActive = True  

## დავალიანება გრაფიკზე  

Schedule.AmountDebt  

## ინვოისის დაგენერირებული თანხა  

InvoiceBase.Amount  

## ტრანშის თანხა  

Schedule.InstallmentAmount  

## გაუქმებული თანხა  

Schedule.CancelledAmount  

## გადახდილი თანხა  

Schedule.PaidAmount  

## პოლისის პრემიის ვალუტა  

PolicyBase.GrossWrittenPremiumCurrency  

## ტრანშის თარიღი  

Schedule.InstalmentDate  

## პოლისის ლიცენზია  

Policybase.License  

## საბეჭდი ფორმა ინოისი PA  

ReportData.Oid = 172

## Feature: 3.TBCI.1  

## პოლისის ტიპი არის კასკო  

Policybase.License = 2  

## სადაზღვევო შემთხვევა არის მოტორის ტიპის  

PotentialClients.Module.BusinessObjects.Claims.MotorInsuranceAccident  

## მზად არის სატელეფონო დარეგულირებისთვის  

ReportedClaim.StateMachineStatusEx.Id = 146  

## ასანაზღაურებელი თანხა  

ReportedClaim.SettledClaim.AmountPayable  

## პოლისის მოსარგებლე  

ReportedClaim.InsuranceAccident.Policybase.Benefitiary  

## კონტრაგენტი არის იურიდიული პირი  

ContrahentBase.ClientType = 1  

## კონტრაგენტი არის შავ სიაში  

ContrahentBase.BlackListPerson = True  

## თანხის მიმღები  

settledClaim.SettlementChargesCollection.Contrahent  

## Description -ში გადმოცემული ტექსტი  

Response.Description  

## ზარალი არის დამტკიცებულია სტატუსში  

{ 35, 70, 71, 72, 77, 146 }.Any(a => a == reportedClaim.StateMachineStatusEx.ID)  

## პოლისის ტიპი არის ქონება  

Policybase.License = 24  

## Settlement transferred call  

ReportedClaim.StateMachineStatusEx.Id = 147  

## დარეგულირებულია სატ.ზარით  

ReportedClaim.StateMachineStatusEx.Id = 135

## ფიზიკური პირი არის ავტომობილის მესაკუთრე

BasePerson.IsUnknownBeneficiary != True

--ToDo  

## Feature: 50.TBCI.2  

## პირველი ტრანში  

Schedule.OrdinaryNumber = 1  

## ტრანშის გადახდა  

InstalmentAmount * 0.9 < PaidAmount + setoffamount + writeoffamount  

## მიმართულება  

Policybase.Direction  

## მესამე პირის პასუხისმგებლობის ზარალები (MTPL)  

როდესაც TBC-ში დაზღვული ავტომობილი შეჯახებისას ახდენს სხვისი ქონების დაზიანებას და დაზღვეულის პოლისი ითვალისწინებს მესამე პირის პასუხისმგებლობის დაზღვევას (MTPL)  
ამ შემთხვევაში ზარალი რეგისტრირდება MTPL-ის პოლისზე.

MTPL-ის პოლისი არსებობს 2 ტიპის:

- როდესაც დაზღვეულს შეძენილი აქვს Motor-ის პოლისი, ანუ აზღვევს საკუთარ ავტომობილს და ამავდრულად მესამე პირის მიმართ პასუხისმგებლობას (ამ შემთხვევაში პოლისის ლიცენზიის ID = 3)

- როდესაც დაზღვეული ყიდულობს მხოლოდ მესამე პირის მიმართ პასუხისმგებლობას Standalone MTPL - (ამ შემთხვევაში პოლისის ლიცენზიის ID = 37)

```sql
select r.id, p.policynumber, i.descriptioneng from reportedclaim r
  join policybase p on p.id = r.policybase
  join insurancelicence i on p.license = i.id
where i.descriptioneng = 'MTPL'
limit 10;
```

ეს მონაცემები ივსება შემდეგ ველებში:

```sql
select r.id,
       p.policynumber,
       r.damagedobjectname "დაზიანებული ქონების დასახელება",
       r.damagedobjectcode "დაზიანებული ქონების საიდენტიფიკაციო",
       r.damagedobjectnumber "დაზიანებული ქონების ნომერი",
       r.damagedobjectowner "დაზიანებული ქონების მფლობელი",
       r.damagedobjectownernumber "დაზიანებული ქონების მფლობელის საკონტაქტო",
       d.name "დაზიანებული ქონების ტიპი"
from reportedclaim r
  join policybase p on p.id = r.policybase
  join insurancelicence i on p.license = i.id
  left join damagedobjecttype d on r.damagedobjecttype = d.id
where p.license in (3,37)
      and r.id = 71872;
```

მაგრამ ეს ველები არის varchar ტიპის, არ აქვს ვალიდაციები და შესაბამისად მხოლოდ ზოგადი ინფორმატიული ხასიათი აქვს.  
თუ საჭიროა რომ პიროვნება იყოს იდენტიფიცირებული, მაშინ უმჯობესია ინფორმაცია აიღოთ ზარალის ანაზღაურების შემთხვევაში თანხის მიმღების დეტალებიდან.

ასევე შესაძლოა არსებობდეს შემთხვევები, როდესაც დაზიანებული ქონების მფლობელი იყოს სხვა პიროვნება და თანხის მიმღები სხვა. მაგ. თანხის მიმღები იყოს სადაზღვევო კომპანია, ან უბრალოდ სხვა პიროვნება

```sql
select r.id,
       p.policynumber,
       r.damagedobjectname "დაზიანებული ქონების დასახელება",
       r.damagedobjectcode "დაზიანებული ქონების საიდენტიფიკაციო",
       r.damagedobjectnumber "დაზიანებული ქონების ნომერი",
       r.damagedobjectowner "დაზიანებული ქონების მფლობელი",
       r.damagedobjectownernumber "დაზიანებული ქონების მფლობელის საკონტაქტო",
       d.name "დაზიანებული ქონების ტიპი",
       c.idcodebase "თანხის მიმღების პირადი ნომერი",
       c.fullname "თანხის მიმღების სახელი და გვარი"
from reportedclaim r
  join policybase p on p.id = r.policybase
  join insurancelicence i on p.license = i.id
  left join damagedobjecttype d on r.damagedobjecttype = d.id
  join settledclaim s on s.reportedclaim = r.id
  join settlementcharge sc on sc.settledclaim = s.id
  join contrahentbase c on sc.contrahent = c.id
where p.license in (3,37)
      and r.id = 55258;
```
