# VSCode required extensions

|Extension  |Description|
|---------|---------|
|[Docs Authoring Pack](https://marketplace.visualstudio.com/items?itemName=docsmsft.docs-authoring-pack)    | Provides assistance in authoring .md files|
|[Paste Image](https://marketplace.visualstudio.com/items?itemName=mushan.vscode-paste-image)     | Makes pasting images into the code possible with Ctrl+Alt+V, instead of Ctrl+V|
